<?php

namespace App;

use Session;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    static public function cartTotals()
    {
      $total = [];
      $gst = env('GST');
      $pst = env('PST');

      if(Session::has('cart'))
      {
        $cart = Session::get('cart');

        foreach($cart as $item){
        $price = $item['price'] * $item['qty'];
        //push the price item to your subtotal array
        array_push($total, $price);
        }

        $subTotal = array_sum($total);
        $g_tax = $subTotal * $gst;
        $p_tax = $subTotal * $pst;
        $finalTotal = $subTotal + $g_tax + $p_tax;

        $cartTotal = [];
        $cartTotal['subTotal'] = $subTotal;
        $cartTotal['g_tax'] = $g_tax;
        $cartTotal['p_tax'] = $p_tax;
        $cartTotal['finalTotal'] = $finalTotal;

        return $cartTotal;

    }
  }
}
