<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Product;
use Cart;
use \Session;

class CartsController extends Controller
{
  public $qty = 0;

  public function __contruct()
  {
    if(!Session::has('cart')) {
      Session::put('cart', []);
    }
  }

  /**
 * This method gets the contents of the shopping cart
 * and assigns them to a variable $cart. The method then returns
 * the variable to the chopping-cart.blade.php view
 * @return      the $cart to shopping-cart.blade.php
 * @see         contents of the cart
 */
  public function show()
  {
    $cart = Session::get('cart');
    return view('/shopping-cart', compact('cart'));
  }

  /**
 * This method validates and requests the quantity and id of the product
 * and creates an $item array that grabs certain fields from the database.
 * We then get the cart and request the id from that $item and put it in
 * the cart.
 * @return      to /plants
 */
  public function store(Request $request)
  {
    $this->validate($request, [
      'qty' => 'required',
      'id' => 'required|min:1'
    ]);

    $plant = Product::find($request->input('id'));
    $price = $plant->price;
    if($request->input('qty') > 0) 
    {
      $line_price = $request->input('qty') * $price;
    } 
    else
    {
      $line_price = $price;
    }
    
    $item = [
      'name' => $plant->name,
      'id' => $plant->id,
      'price' => $plant->price,
      'image' => $plant->file_name,
      'qty' => $request->input('qty'),
      'line_price' => $line_price
    ];

    $cart = Session::get('cart');
    $cart[$request->input('id')] = $item;
    Session::put('cart', $cart);
    return redirect('/plants');
  }

  /**
 * This method will delete items from the cart. If we have
 * a cart and we get the cart, then we'll assign it to $cart.
 * for each $product in the cart we will check if the product's
 * 'id' is e
 *
 */
  public function delete($id)
  {
    if ( Session::has( 'cart' ) && is_array( Session::get('cart') ) ) {
      $cart = Session::get('cart');
      unset($cart[$id]);
    }
      Session(['cart' => $cart]);
      return redirect('/cart');
  }

  public function update(Request $request)
  {
      $cart = Session::get('cart');
      $id = $request->input('id');

      if($request->input('increment') == 'add') {
        $cart[$id]['qty'] += 1;
      }
      else {
        if($cart[$id]['qty'] > 0)
          {
            $cart[$id]['qty'] -= 1;
          }
      }

      $cart[$id]['line_price'] = ($cart[$id]['price'] * $cart[$id]['qty']);
      Session(['cart' => $cart]);
      return redirect('/cart');
  }

  public function subTotal()
  {
    $total = [];
    $gst = env('GST');
    $pst = env('PST');

    if(Session::has('cart'))
    {
      $cart = Session::get('cart');

      foreach($cart as $item){
      $price = $item['price'] * $item['qty'];
      //push the price item to your subtotal array
      array_push($total, $price);
      }

      $subTotal = array_sum($total);

      $g_tax = $subTotal * $gst;
      $p_tax = $subTotal * $pst;
      $finalTotal = $subTotal + $g_tax + $p_tax;
      Session::save('cart');

      return view('cart/shopping-cart')->with(array(
       'cart'    => $cart,
       'subTotal' => $subTotal,
       'finalTotal' => $finalTotal,
       'gst' => $g_tax,
       'pst' => $p_tax
      ));
    }
  else
  {
    Session::flash('fail', 'Your shopping cart is empty');
    $cart = [];
    return view('cart/shopping-cart', compact('cart'));
  }
 }
}
