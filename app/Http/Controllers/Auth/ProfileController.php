<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use Session;

class ProfileController extends Controller
{

/**
 * Description: Show Profile
 * @return type
 */
    public function show()
    {
      if(Auth::check())
      {
    	  $user = Auth::user();
    	 return view('/auth/profile', compact('user'));
      }
      else
      {
         return redirect('/login');
      }
        
      
    }

/**
 * Description: Edit Profile
 * @return type
 */
    public function edit()
    {
      if(Auth::check())
      {
    	  $user = Auth::user();
    	  return view('/auth/editprofile', compact('user'));
      }
      else
      {
        return redirect('/login');
      }
    }

/**
 * Description: Update Profile
 * @param Request $request 
 * @return type
 */
   public function update(Request $request)
   {

    if(Auth::check())
      {
                 

           // Validate
            $this->validate($request,[
          'first_name' => 'required|min:3|max:255',
          'last_name' => 'required|min:3|max:255',
          'phone' => 'required|regex:/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/',
            'street' => 'required|min:3',
            'city' => 'required|min:3|max:255',
            'province' => 'required|min:2|max:255',
            'country' => 'required|min:3|max:255',
            'postal_code' => 'required|min:6|regex:/^([A-Za-z][\d][A-Za-z])[-\s\.]?([\d][A-Za-z][\d])$/'
             ]);
      
         // Update all fields with new values
         $user = User::find($request->id); 
         $user->first_name = $request->input('first_name');
         $user->last_name = $request->input('last_name');
         $user->phone = $request->input('phone');
         $user->street = $request->input('street');
         $user->city = $request->input('city');
         $user->province = $request->input('province');
         $user->country = $request->input('country');
         $user->postal_code = $request->input('postal_code');
         $user->save();

         session::flash('success', 'Your profile updated successfully');
          return redirect('/profile');
       }
       else
       {
        return view('/login');
       }
      
 	 
       
   }

}
