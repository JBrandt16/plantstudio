<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Session;


class ChangePasswordController extends Controller
{
   	/**
     * Load change password form and show user data
     * @return type
     */
    public function show()
    {
    	$user = Auth::user();
    	return view('/auth/passwords/changepass', compact('user'));
    }

    public function store(Request $request)
     {
      

    	$user = Auth::user();
	    $this->validate($request,[
   	 	'password' => 'required|min:8|regex:/(?=.*[A-Z]+)(?=.*[a-z]+)(?=.*[\d]+)(?=.*[\!\@\#\$\&\*\(\)\-\+]+)[A-Za-z\d\!\@\#\$\&\*\(\)\-\+]{8,}/|confirmed',
   	     ]);
  
   	 	$user->password = bcrypt($request->input('password'));

		if($user->update()){
      		session::flash('success', 'Password successfully changed');
      		return redirect('/profile');
     	} else{
      		session::flash('fail');
      		return redirect('/profile');
     	}
       
   }


}
