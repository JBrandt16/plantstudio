<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Product;

class PlantsController extends Controller
{
	public function index()
  {
    $title = 'Plants';
    $plants = \DB::table('product')->paginate(10);
    $categories = \DB::table('category')->get();
    return view('plants/index',compact('title','plants','categories'));
  }
  
    public function show($id) 
    {
    	$title = 'A Plant';
    	$plant = \DB::table('product')->find($id);
      $review = \DB::table('review')->where('product_id', $id)->first();
    	return view('plants.show', compact('title', 'plant', 'review'));
    }

    public function search(Request $request)
    {
      $q = $request->input('q');
      $plants = Product::where ('name', 'like', '%' . $q . '%')->orWhere ('long_description','like','%' . $q . '%')->orWhere ('short_description','like','%' . $q . '%')-> paginate(10);

      $categories = \DB::table('category')->get();
      return view('plants/index',compact('plants','categories'));
    }

    /*Query used for category selection*/
    /*Query used for category selection*/
    public function category($category)
    {
      $plants = \DB::table('product')
                     ->join('category_product','product.id','=','category_product.product_id')
                     ->select('product.*')
                     ->where('category_product.category_id','=', $category)
                     ->paginate(10);
      $categories = \DB::table('category')->get();
      return view('plants/index',compact('plants','categories'));
    }


}
