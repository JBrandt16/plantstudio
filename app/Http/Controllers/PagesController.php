<?php

/*
 *  Course   : eCOMMERCE PHP Programming
 *  Instuctor: Steve George
 *  Student  : WDD Group
 * 
 * @param about(),contact()and project() function.
 * @return view loading about,project(faq) and contact page.
 * @param namespace App\Http\Controllers
 * @param class named PagesController extends Controller
*/

/**
   * Returns output of view
   * @param string $title
   * @param namespace App\Http\Controllers
   * @return view by using compact method.
   */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\User;
use App\Http\Controllers\Controller;


class PagesController extends Controller
{
    public function index()
    {
      $plants = \DB::table('product')->get();
      return view('index', compact('plants'));
    }

    function about(){
      return view('pages/about');
    
    }
    
    function project(){
      return view('pages/faq');
    
    }
    
    function contact(){
      return view('pages/contact');
    
    }

    function privacy(){
      return view('pages/privacy');
    
    }

    function terms(){
      return view('pages/terms');
    
    }

     public function send(Request $request) {
    $rules_contact = [
      'first_name' => 'required|max:50',
      'last_name' => 'required|max:50',
      'email' => 'required|email',
      'subject' =>'required|max:255'

    ];

    $this->validate($request, $rules_contact);
    $msg= $request->input('first_name') . ' ' . $request->input('last_name') . "\n";
    $msg.= $request->input('email') . "\n" ;
    $msg.= $request->input('subject');


  Mail::raw('Sending emails with Mailgun and Laravel is easy!', function($message) use($msg)

  { $message->subject('Help Request');
    $message->to('stan291993@gmail.com');
    $message->setBody($msg);



  });
  return redirect('/contact');
  }
}
