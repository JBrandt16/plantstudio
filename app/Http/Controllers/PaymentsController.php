<?php

namespace App\Http\Controllers;

define('_5BX_API_LOGIN_ID', '5664331');
define('_5BX_API_KEY', 'RMQVOgXHNwWO81Qutfci98k2uFf83A0PQrC2n2D0nfCAfKQO26KFtMQLr6QQnrOE');

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Payment;
use App\_5bx;
use \Session;
use App\Cart;
use \Exception;
use App\Order;
use Illuminate\Support\Facades\Auth;

class PaymentsController extends Controller
{

  public function p_form(Request $request)
  {

    if(Auth::check())
    {
      $rules = [
        'card_name' => 'required|max:30',
        'card_number' => 'required|numeric|digits:16',
        'card_type' => 'required',
        'expiry_date' => 'required|numeric|digits:4',
        'cvv2' => 'required|numeric|digits:3',
        'first_name' => 'required',
        'last_name' => 'required',
        'email' => 'required',
        'phone' => 'required',
        'street' => 'required',
        'city' => 'required',
        'province' => 'required',
        'country' => 'required',
        'postal_code' => 'required'
      ];
      $this->validate($request, $rules);

      $order = new Order;
      $cartTotal = Cart::cartTotals();
      $order->users_id = Auth::user()->id;
      $order->transaction_status = 'complete';
      $order->shipping_status = 'pending';
      $order->first_name = $request->input('first_name');
      $order->last_name = $request->input('last_name');
      $order->email = $request->input('email');
      $order->phone = $request->input('phone');
      $order->street = $request->input('street');
      $order->city = $request->input('city');
      $order->province = $request->input('province');
      $order->country = $request->input('country');
      $order->postal_code = $request->input('postal_code');
      $order->subtotal = $cartTotal['subTotal'];
      $order->gst = $cartTotal['g_tax'];
      $order->pst = $cartTotal['p_tax'];
      $order->total = $cartTotal['finalTotal'];
      $order->save();
      $cart = Session::get('cart');

      foreach($cart as $item)
      {
        DB::table('line_item')->insert([
          'order_id' => $order->id,
          'product_id' => $item['id'],
          'name' => $item['name'],
          'price' => $item['price'],
          'qty' => $item['qty']
        ]);

      }



      try {

        $transaction = new \App\_5bx;
    	  $transaction->amount($order->total);
    	  $transaction->card_num($request->input('card_number')); // credit card number
    	  $transaction->exp_date($request->input('expiry_date')); // expiry date month and year
    	  $transaction->cvv($request->input('cvv2')); // card cvv number
    	  $transaction->ref_num('666'); // your reference or invoice number
    	  $transaction->card_type($request->input('card_type')); // card type
    	  $response = $transaction->authorize_and_capture();

      } catch (Exception $e) {
        die($e->getMessage());
      }

      $trans = $response->transaction_response;
      $user = Auth::user();

      if($trans->response_code == '1')
      {
        Session::forget('cart');
        return view('cart/thankyou', compact('order', 'trans', 'user', 'transaction', 'cart', 'item_total'));
      }
      else {
        Session::flash('fail', 'Sorry, invalid credit card.');
        return view('cart/payment', compact('user'));
      }

    }

  }

  public function g_form()
  {
    if(Auth::check())
    {
      $user = Auth::user();
      return view('cart/payment', compact('user'));
    }
    else
    {
      return redirect('/login');
    }
  }
}
