<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
	public function categories() 
    {
        if(Auth::check())
            {
                if(Auth::user()->is_admin == 1)
                {
                    $title = 'Admin Categories';
                    $categories = \DB::table('category')->get();
                    return view('admin.category.index', compact('title', 'categories'));
                } else
                {
                    echo "No privileges";
                }
            }
        else 
        {
            return redirect('/login');
        }
        
    } // products

    public function edit_category($id) 
    {
        if(Auth::check())
            {
                if(Auth::user()->is_admin == 1)
                {
                    $title = 'Edit Categories';
                    $category = \DB::table('category')->find($id);
                    return view('admin.category.show', compact('title', 'category'));
                } else
                {
                    echo "No privileges";
                }
            }
        else 
        {
            return redirect('/login');
        }

       
    } // edit_product

    public function update_category(Request $request) 
    {
        if(Auth::check())
            {
                if(Auth::user()->is_admin == 1)
                {
                    $this->validate(request(),([
                        'name' => 'required|regex:/^[A-z\s\ç\ã\â\ê\í\ô\-\,\_]*$/'
                    ]));

                    $category = \DB::table('category')
                    ->where('id', $request->input('id'))
                    ->update([
                        'name' => $request->input('name')
                    ]);
                    return redirect('/admin/category');
                } else
                {
                    echo "No privileges";
                }
            }
        else 
        {
            return redirect('/login');
        }
        
    } // update_product

    public function add_category(Request $request)
    {
        if(Auth::check())
            {
                if(Auth::user()->is_admin == 1)
                {
                    $title = 'Add a New Category';
                    return view('admin.category.new_category', compact('title'));
                } else
                {
                    echo "No privileges";
                }
            }
        else 
        {
            return redirect('/login');
        }
        
    } // add_product

    public function store_category(Request $request)
    {
        if(Auth::check())
            {
                if(Auth::user()->is_admin == 1)
                {
                    $this->validate(request(),([
                    'name' => 'required|regex:/^[A-z\s\ç\ã\â\ê\í\ô\-\,\_]*$/'
                ]));

                $category = \DB::table('category')
                            ->insert([
                                'name' => $request->input('name')
                            ]);
                return redirect('/admin/category');
                } else
                {
                    echo "No privileges";
                }
            }
        else 
        {
            return redirect('/login');
        }
        
    } // store_product

    public function delete_category($id)
    {
        $category = \DB::table('category')
                        ->where('id', $id)
                        ->delete();
        return redirect('/admin/category');
    }
}
