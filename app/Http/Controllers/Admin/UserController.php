<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
 	/**
     * List all users
     * @return view (users)
     */
    public function users()
    {
        if(Auth::check())
            {
                if(Auth::user()->is_admin == 1)
                {
                    $users = \DB::table('users')->get();
                    return view('admin.users.index', compact('users'));
                } else
                {
                    return view('/denied');
                }
            }
        else 
        {
            return redirect('/login');
        }
        
    } // users

    /**
     * Show user's details
     * @param type $id 
     * @return view
     */
    public function show_user($id)
    {
        if(Auth::check())
            {
                if(Auth::user()->is_admin == 1)
                {
                    $user = \DB::table('users')->find($id);
                    return view('admin.users.show', compact('user'));
                } else
                {
                    return view('/denied');
                }
            }
        else 
        {
            return redirect('/login');
        }
    } // show_user

    /**
     * Update users table
     * @param Request $request 
     * @return view
     */
    public function update_user(Request $request)
    {
        if(Auth::check())
            {
                if(Auth::user()->is_admin == 1)
                {
                    $user = \DB::table('users')
                        ->where('id',$request->input('id'))
                        ->update([
                                'is_admin' => $request->input('is_admin')
                                ]);
                    return redirect('/admin/users/');
                } else
                {
                    return view('/denied');
                }
            }
        else 
        {
            return redirect('/login');
        }
    } // update_user

    /**
     * Delete user 
     * @param type $id 
     * @return view
     */
    public function delete_user($id)
    {
        if(Auth::check())
            {
                if(Auth::user()->is_admin == 1)
                {
                    $user = \DB::table('users')
                        ->where('id', $id)
                        ->delete();
                    return redirect('/admin/users/');
                } else
                {
                    return view('/denied');
                }
            }
        else 
        {
            return redirect('/login');
        }
    } // delete_user
}
