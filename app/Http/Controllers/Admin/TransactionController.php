<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class TransactionController extends Controller
{
    /**
     * List all transactions
     * @return view transaction/index
     */
    public function transactions()
    {
        if(Auth::check())
            {
                if(Auth::user()->is_admin == 1)
                {
                    $transactions = \DB::table('transaction')->get();
                    return view('admin.transaction.index', compact('transactions'));
                } else
                {
                    return view('/denied');
                }
            }
        else 
        {
            return redirect('/login');
        }
        
    } // transactions
}
