<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class SupplierController extends Controller
{
	public function suppliers() 
    {
        if(Auth::check())
            {
                if(Auth::user()->is_admin == 1)
                {
                    $title = 'Admin Suppliers';
                    $suppliers = \DB::table('supplier')->get();
                    return view('admin.supplier.index', compact('title', 'suppliers'));
                } else
                {
                    echo "No privileges";
                }
            }
        else 
        {
            return redirect('/login');
        }
        
    } // products

    public function edit_supplier($id) 
    {
        if(Auth::check())
            {
                if(Auth::user()->is_admin == 1)
                {
                    $title = 'Edit Supplier';
                    $supplier = \DB::table('supplier')->find($id);
                    return view('admin.supplier.show', compact('title', 'supplier'));
                } else
                {
                    echo "No privileges";
                }
            }
        else 
        {
            return redirect('/login');
        }

       
    } // edit_product

    public function update_supplier(Request $request) 
    {
        if(Auth::check())
            {
                if(Auth::user()->is_admin == 1)
                {
                    $this->validate(request(),([
                        'name' => 'required|string|regex:/^[A-z\s\ç\ã\â\ê\í\ô\-\,\_]*$/',
                        'phone' => 'required|numeric',
                        'email' => 'required|email'
                    ]));

                    $supplier = \DB::table('supplier')
                    ->where('id', $request->input('id'))
                    ->update([
                        'name' => $request->input('name'),
                        'phone' => $request->input('phone'),
                        'email' => $request->input('email')
                    ]);
                return redirect('/admin/supplier');
                } else
                {
                    echo "No privileges";
                }
            }
        else 
        {
            return redirect('/login');
        }
        
    } // update_product

    public function add_supplier(Request $request)
    {
        if(Auth::check())
            {
                if(Auth::user()->is_admin == 1)
                {
                    $title = 'Add a New Supplier';
                    return view('admin.supplier.new_supplier', compact('title'));
                } else
                {
                    echo "No privileges";
                }
            }
        else 
        {
            return redirect('/login');
        }
        
    } // add_product

    public function store_supplier(Request $request)
    {
        if(Auth::check())
            {
                if(Auth::user()->is_admin == 1)
                {
                    $this->validate(request(),([
                    'name' => 'required|string|regex:/^[A-z\s\ç\ã\â\ê\í\ô\-\,\_]*$/',
                    'phone' => 'required|numeric',
                    'email' => 'required|email'
                ]));

                $supplier = \DB::table('supplier')
                            ->insert([
                                'name' => $request->input('name'),
                                'phone' => $request->input('phone'),
                                'email' => $request->input('email')
                            ]);
                return redirect('/admin/supplier');
                } else
                {
                    echo "No privileges";
                }
            }
        else 
        {
            return redirect('/login');
        }
        
    } // store_product

    public function delete_supplier($id)
    {
    	$supplier = \DB::table('supplier')
                        ->where('id', $id)
                        ->delete();
                    return redirect('/admin/supplier');
    }
}
