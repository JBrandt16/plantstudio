<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
	/**
     * Load the admin dashboard
     * @return type view
     */
  	public function home()
    {
         if(Auth::check())
            {
                if(Auth::user()->is_admin == 1)
                {
                    
                    //$categories = \DB::table('category')->get();
                    $products = \DB::table('product')->count();
                    $users = \DB::table('users')->count();
                    $admin = \DB::table('users')->where('is_admin', '1')->count();
                    $categories = \DB::table('category')->count();
                    $average_price = \DB::table('product')->avg('price');
                    $highest_price = \DB::table('product')->max('price');
                    $lowest_price = \DB::table('product')->min('price');
                    $transactions = \DB::table('transaction')->count();
                    $suppliers = \DB::table('supplier')->count();
                    $orders = \DB::table('orders')->count();
                    return view('admin/index', compact('products', 'users', 'admin', 'categories', 'average_price', 'highest_price', 'lowest_price', 'transactions', 'suppliers','orders'));  
                } else
                {
                    return view('/denied');
                }
            }
        else 
        {
            return redirect('/login');
        }
    } // home
}
