<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function orders()
    {
        if(Auth::check())
            {
                if(Auth::user()->is_admin == 1)
                {
                    $orders = \DB::table('orders')->get();
                    return view('admin.order.index', compact('orders'));
                } else
                {
                    return view('/denied');
                }
            }
        else 
        {
            return redirect('/login');
        }
        
    } // orders
}
