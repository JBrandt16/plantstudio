<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Product;

class ProductController extends Controller
{
	/**
     * List all products in admin page
     * @return type view
     */
    public function products() 
    {
        if(Auth::check())
            {
                if(Auth::user()->is_admin == 1)
                {
                    $title = 'Admin Products';
                    $plants = \DB::table('product')->get();
                    return view('admin.products.index', compact('title', 'plants'));
                } else
                {
                    return view('/denied');
                }
            }
        else 
        {
            return redirect('/login');
        }
        
    } // products

    /**
     * Load form to edit products
     * @param type $id 
     * @return type view
     */
    public function edit_product($id) 
    {
        if(Auth::check())
            {
                if(Auth::user()->is_admin == 1)
                {
                    $plant = \DB::table('product')->find($id);
                    $suppliers = \DB::table('supplier')->get();
                    $categories = \DB::table('category')->get();
                    $cat_prod = \DB::table('category_product')->get();
                    return view('admin.products.show', compact('plant', 'suppliers', 'categories', 'cat_prod'));
                } else
                {
                    return view('/denied');
                }
            }
        else 
        {
            return redirect('/login');
        }

       
    } // edit_product

    /**
     * Update products in database
     * @param Request $request 
     * @return type view
     */
    public function update_product(Request $request) 
    {
        if(Auth::check())
            {
                if(Auth::user()->is_admin == 1)
                {
                    $this->validate(request(),([
                    'name' => 'required|string|regex:/^[A-z\s\ç\ã\â\ê\í\ô\-\,\_]*$/',
                    'file_name' => 'required',
                    'seed' => 'required|boolean',
                    'price' => 'required',
                    'quantity' => 'required|numeric',
                    'short_description' => 'required',
                    'long_description' => 'required', 
                    'supplier_id' => 'required|numeric',
                    'categories' => 'required',
                    'published' => 'required|boolean'
                ]));
                    
                    $product = Product::find($request->id);
                    $product->name = $request->input('name');
                    $product->file_name = $request->input('file_name');
                    $product->seed = $request->input('seed');
                    $product->price = $request->input('price');
                    $product->quantity = $request->input('quantity');
                    $product->short_description = $request->input('short_description');
                    $product->long_description = $request->input('long_description');
                    $product->supplier_id = $request->input('supplier_id');
                    $product->published = $request->input('published');
                    $product->save();
                    $product->categories()->sync($request->input('categories'));

                    return redirect('/admin/products');
                } else
                {
                    return view('/denied');
                }
            }
        else 
        {
            return redirect('/login');
        }
        
    } // update_product

    /**
     * Load form to add new product
     * @param Request $request 
     * @return type view
     */
    public function add_product(Request $request)
    {
        if(Auth::check())
            {
                if(Auth::user()->is_admin == 1)
                {
                    $suppliers = \DB::table('supplier')->get();
                    $categories = \DB::table('category')->get();
                    return view('admin.products.new_product', compact('suppliers', 'categories'));
                } else
                {
                    return view('/denied');
                }
            }
        else 
        {
            return redirect('/login');
        }
        
    } // add_product

    /**
     * Insert new products in database
     * @param Request $request 
     * @return type view
     */
    public function store_products(Request $request)
    {
        if(Auth::check())
            {
                if(Auth::user()->is_admin == 1)
                {
                    $this->validate(request(),([
                    'name' => 'required|string|regex:/^[A-z\s\ç\ã\â\ê\í\ô\-\,\_]*$/',
                    'file_name' => 'required',
                    'seed' => 'required|boolean',
                    'price' => 'required',
                    'quantity' => 'required|numeric|min:0|max:200',
                    'short_description' => 'required',
                    'long_description' => 'required', 
                    'supplier_id' => 'required|numeric',
                    'categories' => 'required',
                    'published' => 'required|boolean'
                ]));

                $product = new Product;
                $product->name = $request->input('name');
                $product->file_name = $request->input('file_name');
                $product->seed = $request->input('seed');
                $product->price = $request->input('price');
                $product->quantity = $request->input('quantity');
                $product->short_description = $request->input('short_description');
                $product->long_description = $request->input('long_description');
                $product->supplier_id = $request->input('supplier_id');
                $product->published = $request->input('published');
                $product->save();
                $product->categories()->sync($request->input('categories'));

                
                return redirect('/admin/products/');
                } else
                {
                    return view('/denied');
                }
            }
        else 
        {
            return redirect('/login');
        }
        
    } // store_product

        /**
     * Delete products from database 
     * @param type $id 
     * @return view
     */
    public function delete_product($id)
    {
        if(Auth::check())
            {
                if(Auth::user()->is_admin == 1)
                {
                    $product = \DB::table('product')
                        ->where('id', $id)
                        ->delete();
                    $category = \DB::table('category_product')
                        ->where('product_id', $id)
                        ->delete();
                    return redirect('/admin/products/');
                } else
                {
                    return view('/denied');
                }
            }
        else 
        {
            return redirect('/login');
        }
    } // delete_product
}
