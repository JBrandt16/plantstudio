<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ReviewController extends Controller
{
	/**
	 * List all reviews
	 * @return view review/index
	 */
    public function reviews()
    {
        if(Auth::check())
            {
                if(Auth::user()->is_admin == 1)
                {
                    $reviews = \DB::table('review')->get();
                    $users = \DB::table('users')->get();
                    $products = \DB::table('product')->get();
                    return view('admin.review.index', compact('reviews', 'users', 'products'));
                } else
                {
                    return view('/denied');
                }
            }
        else 
        {
            return redirect('/login');
        }
        
    } // reviews

    /**
     * Show review's details
     * @param type $id 
     * @return view
     */
    public function show_review($id)
    {
        if(Auth::check())
            {
                if(Auth::user()->is_admin == 1)
                {
                    $review = \DB::table('review')->find($id);
                    $users = \DB::table('users')->get();
                    $products = \DB::table('product')->get();
                    return view('admin.review.show', compact('review', 'users', 'products'));
                } else
                {
                    return view('/denied');
                }
            }
        else 
        {
            return redirect('/login');
        }
    } // show_review

    /**
     * Update reviews table
     * @param Request $request 
     * @return view
     */
    public function update_review(Request $request)
    {
        if(Auth::check())
            {
                if(Auth::user()->is_admin == 1)
                {
                    $review = \DB::table('review')
                        ->where('id',$request->input('id'))
                        ->update([
                                'published' => $request->input('published')
                                ]);
                    return redirect('/admin/reviews/');
                } else
                {
                    return view('/denied');
                }
            }
        else 
        {
            return redirect('/login');
        }
    } // update_user

    /**
     * Delete review 
     * @param type $id 
     * @return view
     */
    public function delete_review($id)
    {
        if(Auth::check())
            {
                if(Auth::user()->is_admin == 1)
                {
                    $review = \DB::table('review')
                        ->where('id', $id)
                        ->delete();
                    return redirect('/admin/reviews/');
                } else
                {
                    return view('/denied');
                }
            }
        else 
        {
            return redirect('/login');
        }
    } // delete_review
}
