  @extends('layouts.plants_layout')

  @section('content')
  <div id= "main" class="container" ><!--main content -->
    <div class="row">
      <div id="pic" class="col-sm-4">
        <h2>PlantStudio <br /> Privacy Statement</h2>
      </div>
      <div id= "pic1"  class="col-sm-8">
        <h3>Plant Studio</h3>
        <p>PlantStudio respects your privacy. This Privacy Statement describes how PlantStudio collects, uses, shares and protects information from and about our customers, visitors and job applicants, along with choices you can make about such information. It also contains other important privacy disclosures, such as how we may update the Statement and how you can contact us.</p>
        <h3>Scope</h3>
        <p>This Privacy Statement applies to the Canada practices of PlantStudio Companies, Inc. and its Canadian operating subsidiaries and affiliates except as outlined below. It applies to most interactions with our customers, visitors, and job applicants, including:</p>
          <ul>
            <li>Use of our websites that point to this Statement</li>
            <li>Use of our mobile applications</li>
            <li>Phone, email and other electronic or written communications</li>
            <li>Visits to our stores or other in-person events</li>
            <li>Social media interactions</li>
            <li>Surveys, contests and sweepstakes</li>
            <li>Use of our in-store Wi-Fi</li>
            <li>Online advertisements</li>
          </ul>
          
        <h3>Information We Collect by Automated Means Online</h3>
<p>When you use our websites or applications (“online services”), we may collect certain information by automated means, using technologies such as cookies, web server logs and web beacons. This information may be used to provide a better-tailored shopping experience, and for market research, data analytics and system administration purposes. </p>

<p>Our online services may use HTTP cookies, HTML5 cookies, Flash cookies and other types of local storage (such as browser-based or plugin-based local storage). Cookies are small text files that websites send to your computer or other internet-connected device to uniquely identify your browser or to store information or settings.</p>

      </div>
    </div>
  </div>
@endsection
