@extends('layouts.plants_layout')

@section('content')

<div class="container main_content"><!--main content -->
<div class="row">

  <div class="col-md-4">
   <div class="panel panel-default">
        <h2>Contact us</h2>
       <div class="panel-body">
          <form action="/contact" method="post" class="form-horizontal">
          {{ csrf_field() }}
          <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                          <label for="first_name" class="col-md-4 control-label">First Name</label>

                              <div class="col-md-8">
                              <input class="first_name form-control" type="text" name="first_name" value="{{ old('first_name') }}" required autofocus>

                              @if ($errors->has('first_name'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('first_name') }}</strong>
                                  </span>
                              @endif
                      </div>
                      </div>

       <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                          <label for="last_name" class="col-md-4 control-label">Last Name</label>

                            <div class="col-md-8">
                              <input class="last_name form-control" type="text" name="last_name" value="{{ old('last_name') }}" required autofocus>

                              @if ($errors->has('last_name'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('last_name') }}</strong>
                                  </span>
                              @endif
                        </div>
                      </div>
          <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email" class="col-md-4 control-label">Email</label>

            <div class="col-md-8">
               <input class="email form-control" type="text" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                     @if ($errors->has('email'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('email') }}</strong>
                                  </span>
                              @endif
                       </div>
                      </div>


        <div class="form-group{{ $errors->has('subject') ? ' has-error' : '' }}">
                          <label for="subject" class="col-md-4 control-label">Subject</label>
                          <div class="col-md-8">

                              <input class="subject form-control" type="text" name="subject" value="{{ old('subject') }}" required autofocus>

                              @if ($errors->has('subject'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('subject') }}</strong>
                                  </span>
                              @endif
                       </div>
                      </div>

  <div class="form-group">
                          <div class="col-md-6 col-md-offset-6">
                              <button type="submit" class="btn btn-primary">
                                  Send Form
                              </button>
                          </div>
                      </div>
</form>
</div>
  </div>
</div>

  <div class="col-md-6">

    <h3>Email Form</h3>

    <p>The easiest way to answer your question or resolve your issue is to call us.
       If you still want to use this online form we’ll do our best to respond within 2 business days.</p><br>

       <p><img src="/images/location.png"  width="50" height="50" > Winnipeg, Manitoba</p><br>
       <p><img src="/images/phone.png"  width="50" height="50" >+1431777777</p>




  </div>


</div>


</div>


@endsection