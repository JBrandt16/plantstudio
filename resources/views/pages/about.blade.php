  @extends('layouts.plants_layout')

  @section('content')
  <div id="main" class="container main_content" ><!--main content -->
    <div class="row">
      <div id="pic" class="col-sm-4">
        <h2>About us</h2>
        <img src="images/PlantsStudio(About)_03.jpg" class="img-rounded" alt="Tomato" width="250" height="314">
      </div>
      <div id= "pic1"  class="col-sm-6">
        <h3>PlantStudio</h3>
        <p>The PlantStudio is a plants and accessories store located in Winnipeg Manitoba. 
            They have been in business for the last five years. They sell a variety of plants 
            that they have already started growing in the greenhouse, as well as plants that 
            they import from other countries, they also have packages of seeds for customers 
            to purchase. 
            They also sell accessories that are needed for growing and caring for the plants. 
            Some of the accessories they sell include soil, watering supplies, pots in a variety of sizes,
            and gardening tools. 
        </p>
      </div>
    </div>
  </div>
@endsection
