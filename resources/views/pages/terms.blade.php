  @extends('layouts.plants_layout')

  @section('content')
  <div id= "main" class="container" ><!--main content -->
    <div class="row">
      <div id="pic" class="col-sm-4">
        <h2>PlantStudio <br /> Terms & Conditions</h2>
        
      </div>
      <div id= "pic1"  class="col-sm-8">
        <h3>Introduction</h3>
        <p>These Website Standard Terms And Conditions contained here in on this webpage, shall govern your use of this website, including all pages within this website. These Terms apply in full force and effect to your use of this Website and by using this Website, you expressly accept all terms and conditions contained herein in full. You must not use this Website, if you have any objection to any of these Website Standard Terms And Conditions.</p>
        <h3>Intellectual Property Rights.</h3>
        <p>Other than content you own, which you may have opted to include on this Website, under these Terms, [PlantStudio] and/or its licensors own all rights to the intellectual property and material contained in this Website, and all such rights are reserved.</p>
        
        <p>You are granted a limited license only, subject to the restrictions provided in these Terms, for purposes of viewing the material contained on this Website,</p>
          <h3>Restrictions.</h3>
          <ul>
            <li>publishing any Website material in any media;</li>
            <li>selling, sublicensing and/or otherwise commercializing any Website material;</li>
            <li>publicly performing and/or showing any Website material;</li>
            <li>using this Website in any way that is, or may be, damaging to this Website;</li>
            <li>using this Website in any way that impacts user access to this Website;</li>
            <li>using this Website contrary to applicable laws and regulations, or in a way that causes, or may cause, harm to the Website, or to any person or business entity;</li>
            <li>engaging in any data mining, data harvesting, data extracting or any other similar activity in relation to this Website, or while using this Website;
             using this Website to engage in any advertising or marketing;</li>
          </ul>
      </div>
    </div>
  </div>
@endsection
