  @extends('layouts.plants_layout')

  @section('content')
  <div id= "main" class="container main_content" ><!--main content -->
    <div class="row">
      <div id="pic" class="col-sm-4">
        <h2>FAQ<br />PlantStudio</h2>
      </div>
      <div id= "pic1"  class="col-sm-6">
        <h3>Most Popular Questions</h3>
      <div>
        <!-- content start-->
        <div id="paraf">
          <p><a href =#>What are the best ways to control invasive species including plants, pests and pathogens?
            </a></p>
            </div>
            <div id="mainp">Invasive species are an increasingly significant threat to our environment,
              economy, health and well-being. Most are nonindigenous (evolved elsewhere and accidentally 
              introduced) and have been removed from the constraints regulating growth in their native habitat.
              The best method of control is to prevent establishment in the first place or to quickly identify
              establishment and adopt an eradication programme. However, if an invasive
              species becomes established many of the options for removal can cause environmental damage,
            </div>
        
        <!--para1-->
        
          <div id="paraf1">
                <p><a href =#>How can plants contribute to solving the energy crisis and ameliorating global warming?</a></p>
          </div>
          <div id="mainp1">Plants use solar energy to power the conversion of CO2 into plant materials
            such as starch and cell walls. Plant material can be burnt or fermented to release heat energy 
            or make fuels such as ethanol or diesel. There is interest in using algae (unicellular aquatic plants)
            to capture CO2 emissions from power stations at source. Biomass cellulose crops such as Miscanthus giganteus
            (Poaceae) are already being burnt with coal at power stations. There is understandable distaste for using food crops such as wheat and maize for fuel, but currently 30% of the 
            US maize crop is used for ethanol production, and sustainable solutions are being found. 
          </div>
        
        
          <div id="paraf2">
            <p><a href =#>What new scientific approaches will be central to plant biology in the 21st Century?</a></p>
          </div>
          <div id="mainp2">Biologists now have a good general understanding of the principles of cell 
            and developmental biology and genetics, and how plants function, change, and adapt to their environment.
            Addressing the questions in this list, including those related to generating crops that can deal with
            future challenges, will require detailed knowledge of many more processes and species. New high-throughput 
            technologies for analysing genomes, phenotypes, protein complements, and the biochemical composition of
            cells can provide us with more detailed information in a week 
            than has ever been available before about a particular process, organism or individual. 
          </div>
        <!---content end -->
      </div>
    </div>
  </div>
</div>
@endsection
 
 
 
 

