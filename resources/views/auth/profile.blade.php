@extends('layouts.plants_layout')

@section('content')

@include('partials.flash')



<div class="container main_content">


    <div class="row table-responsive">

        <div class="col-sm-12">

          <table class="table">
                <tr>
                    <th colspan="2" class="h1">{{$user->first_name}} {{$user->last_name}}</th>
                </tr>
                <tr>
                    <th>Email:</th>
                    <th>{{$user->email}}</th>
                </tr>
                <tr>
                    <td>Phone Number: </td>
                    <td>{{$user->phone}}</td>
                </tr>
                <tr>
                    <td>Street: </td>
                    <td>{{$user->street}}</td>
                </tr>
                <tr>
                    <td>City: </td>
                    <td>{{$user->city}}</td>
                </tr>
                <tr>
                    <td>Province: </td>
                    <td>{{$user->province}}</td>
                </tr>
                <tr>
                    <td>Country: </td>
                    <td>{{$user->country}}</td>
                </tr>
                <tr>
                    <td>Postal Code: </td>
                    <td>{{$user->postal_code}}</td>
                </tr>
                
                <tr>
                    <td><a href="/profile/edit" class="btn btn-primary">Edit</a></td>
                    <td><a class="btn btn-warning" href="/change_pass">Change Password</a></td>
                </tr>

            </table>
        </div>

    </div>
</div>


@endsection