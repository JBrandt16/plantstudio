@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="register"><h2>Edit Profile</h2>
                    <h3>{{{ Auth::user()->email }}}</h3>
                </div>

                <!-- Form Model -->
                <div class="panel-body">
                    {!! Form::model($user, ['method' => 'post', 'url' => '/profile'], array('class' => 'col-md-4 control-label')) !!}

                    <div class="form-group">
                        {!! Form::label('first_name', 'First Name', array('class' => 'col-md-4 control-label')) !!}
                        <div class="col-md-6">
                            {!! Form::text('first_name', null, array('class' => 'form-control', 'id'=>'')) !!}
                            @if ($errors->has('first_name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('first_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('last_name', 'Last Name', array('class' => 'col-md-4 control-label')) !!}
                        <div class="col-md-6">
                        {!! Form::text('last_name', null, array('class' => 'form-control', 'id'=>'')) !!}
                        @if ($errors->has('last_name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('last_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
 
                   <div class="form-group">
                        {!! Form::label('phone', 'Phone Number', array('class' => 'col-md-4 control-label')) !!}
                        <div class="col-md-6">
                        {!! Form::text('phone', null, array('class' => 'form-control', 'id'=>'')) !!}
                        @if ($errors->has('phone'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('phone_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>


                    <div class="form-group">
                        {!! Form::label('street', 'Street', array('class' => 'col-md-4 control-label')) !!}
                        <div class="col-md-6">
                        {!! Form::text('street', null, array('class' => 'form-control', 'id'=>'')) !!}
                        @if ($errors->has('street'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('street') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('city', 'City', array('class' => 'col-md-4 control-label')) !!}
                        <div class="col-md-6">
                        {!! Form::text('city', null, array('class' => 'form-control', 'id'=>'')) !!}
                        @if ($errors->has('city'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('city') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('province', 'Province', array('class' => 'col-md-4 control-label')) !!}
                        <div class="col-md-6">
                        {!! Form::text('province', null, array('class' => 'form-control', 'id'=>'')) !!}
                        @if ($errors->has('province'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('province') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('country', 'Country', array('class' => 'col-md-4 control-label')) !!}
                        <div class="col-md-6">
                        {!! Form::text('country', null, array('class' => 'form-control', 'id'=>'')) !!}
                        @if ($errors->has('country'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('country') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('postal_code', 'Postal Code', array('class' => 'col-md-4 control-label')) !!}
                        <div class="col-md-6">
                        {!! Form::text('postal_code', null, array('class' => 'form-control', 'id'=>'')) !!}
                        @if ($errors->has('postal_code'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('postal_code') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    {!! Form::hidden('id') !!}


                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            {!! Form::submit('Update', array('class' => 'btn btn-primary')) !!}

                        </div>
                        
                    </div>

                    {!! Form::close() !!}
                </div><!-- End of form model -->
            </div>
        </div>
    </div>
</div> <!-- End of div container -->

@endsection
