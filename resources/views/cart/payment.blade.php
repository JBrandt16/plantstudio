@extends('layouts.layout')

@section('content')

@include('partials.flash')
	<div id= "main" class="container" ><!--main content -->
          <div class="row">
						<!-- Start of Shipping form -->
            <div class="col-sm-6">
              <h2 class='payment_h2'>Shipping Information</h2>

							<!-- Start of shipping form -->
							{!! Form::model($user,['url' => '/payment', 'method' => 'post', 'id' => 's_form']) !!}


								<!-- First name field -->
								<?php echo Form::label('first_name', 'First Name:', ['class' => 'first_name label']); ?> <br />
								@if($errors->has('first_name'))
									<span class="alert-danger">
										<strong>{{$errors->first('first_name')}}</strong>
									</span>
								@endif
								<?php echo Form::text('first_name', null, array('id' => '', 'class' => 'form-control')); ?> <br />
								<!-- End first name field -->

								<!-- Last name field -->
								<?php echo Form::label('last_name', 'Last Name:', ['class' => 'last_name label']); ?> <br />
								@if($errors->has('last_name'))
									<span class="alert-danger">
										<strong>{{$errors->first('last_name')}}</strong>
									</span>
								@endif
								<?php echo Form::text('last_name', null, array('id' => '', 'class' => 'form-control')); ?> <br />
								<!-- End last name field -->

								<!-- Email address field field -->
								<?php echo Form::label('email', 'E-Mail Address:', ['class' => 'email_addy label']); ?> <br />
								@if($errors->has('email'))
									<span class="alert-danger">
										<strong>{{$errors->first('email')}}</strong>
									</span>
								@endif
								<?php echo Form::text('email', null, array('id' => '', 'class' => 'form-control')); ?> <br />
								<!-- End email address field -->

								<!-- Phone number field -->
								<?php echo Form::label('phone', 'Phone Number:', ['class' => 'phone_number label']); ?> <br />
								@if($errors->has('phone'))
									<span class="alert-danger">
										<strong>{{$errors->first('phone')}}</strong>
									</span>
								@endif
								<?php echo Form::text('phone', null, array('id' => '', 'class' => 'form-control')); ?> <br />
								<!-- End Phone number field -->

								<!-- Street field -->
								<?php echo Form::label('street', 'Street:', ['class' => 'street label']); ?> <br />
								@if($errors->has('street'))
									<span class="alert-danger">
										<strong>{{$errors->first('street')}}</strong>
									</span>
								@endif
								<?php echo Form::text('street', null, array('id' => '', 'class' => 'form-control')); ?> <br />
								<!-- End street field -->

								<!-- City field -->
								<?php echo Form::label('city', 'City:', ['class' => 'city label']); ?> <br />
								@if($errors->has('city'))
									<span class="alert-danger">
										<strong>{{$errors->first('city')}}</strong>
									</span>
								@endif
								<?php echo Form::text('city', null, array('id' => '', 'class' => 'form-control')); ?> <br />
								<!-- End city field -->

								<!-- Province field -->
								<?php echo Form::label('province', 'Province:', ['class' => 'province label']); ?> <br />
								@if($errors->has('province'))
									<span class="alert-danger">
										<strong>{{$errors->first('province')}}</strong>
									</span>
								@endif
								<?php echo Form::text('province', null, array('id' => '', 'class' => 'form-control')); ?> <br />
								<!-- End province field -->

								<!-- Country field -->
								<?php echo Form::label('country', 'Country:', ['class' => 'country label']); ?> <br />
								@if($errors->has('country'))
									<span class="alert-danger">
										<strong>{{$errors->first('country')}}</strong>
									</span>
								@endif
								<?php echo Form::text('country', null, array('id' => '', 'class' => 'form-control')); ?> <br />
								<!-- End country field -->

								<!-- Postal code field -->
								<?php echo Form::label('postal_code', 'Postal Code:', ['class' => 'zip label']); ?> <br />
								@if($errors->has('postal_code'))
									<span class="alert-danger">
										<strong>{{$errors->first('postal_code')}}</strong>
									</span>
								@endif
								<?php echo Form::text('postal_code', null, array('id' => '', 'class' => 'form-control')); ?> <br />
								<!-- End postal code field -->

							<!-- End of shipping form -->

            </div>
            <div class="col-sm-6">
              <h2 class='payment_h2'>Payment Information</h2>

							<!-- Start of payment form -->


								<!-- Card name field -->

								<?php echo Form::label('card_name', 'Card Name:', ['class' => 'card_name label']); ?> <br />
								@if($errors->has('card_name'))
									<span class="alert-danger">
										<strong>{{$errors->first('card_name')}}</strong>
									</span>
								@endif
								<?php echo Form::text('card_name', null, array('id' => '', 'class' => 'form-control')); ?> <br />

								<!-- End card name field -->

								<!-- Card type field -->
								<?php echo Form::label('card_type', 'Card Type:', ['class' => 'card_type label']); ?> <br />
								@if($errors->has('card_type'))
									<span class="alert-danger">
										<strong>{{$errors->first('card_type')}}</strong>
									</span>
								@endif<br />
								<!-- Visa radio -->
								<img src="images/visa.png" alt="visa" />
								<?php echo Form::label('visa', 'Visa', ['class' => 'visa label']); ?>
								<?php echo Form::radio('card_type','visa') ?> <br />
								<!-- End visa radio -->
								<!-- Mastercard radio -->
								<img src="images/mastercard.png" alt="mastercard" />
								<?php echo Form::label('mastercard', 'Master Card', ['class' => 'mastercard label']); ?>
								<?php echo Form::radio('card_type', 'mastercard'); ?> <br />
								<!-- End mastercard radio -->
								<!-- End card name field -->

								<!-- Card number field -->
								<?php echo Form::label('card_number', 'Card Number:', ['class' => 'card_number label']); ?> <br />
								@if($errors->has('card_number'))
									<span class="alert-danger">
										<strong>{{$errors->first('card_number')}}</strong>
									</span>
								@endif
								<?php echo Form::text('card_number', null, array('id' => '', 'class' => 'form-control')); ?> <br />
								<!-- End card number field -->

								<!-- Expiry date field -->
								<?php echo Form::label('expiry_date', 'Expiry Date:', ['class' => 'expiry_date label']); ?> <br />
								@if($errors->has('expiry_date'))
									<span class="alert-danger">
										<strong>{{$errors->first('expiry_date')}}</strong>
									</span>
								@endif <br />
								<?php echo Form::text('expiry_date', null, array('id' => '', 'class' => 'form-control')); ?> <br />
								<!-- Expiry date field -->

								<!-- cvv2 field -->
								<?php echo Form::label('cvv2', 'CVV2:', ['class' => 'cvv2 label']); ?> <br />
								@if($errors->has('cvv2'))
									<span class="alert-danger">
										<strong>{{$errors->first('cvv2')}}</strong>
									</span>
								@endif
								<?php echo Form::text('cvv2', null, array('id' => '', 'class' => 'form-control')); ?> <br />
								<!-- End cvv2 field -->

								<!-- cvv2 field -->
								<?php echo Form::submit('Place Your Order', array('id' => '', 'class' => 'btn btn-primary')); ?>
								<!-- End cvv2 field -->

							{!! Form::close() !!}
							<!-- End of payment form -->
            </div>

          </div>
      </div>

@endsection
