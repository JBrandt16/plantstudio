@extends('layouts.layout')

@section('content')

	<div id= "main" class="container" ><!--main content -->
          <div class="row">
            <div id="pic" class="col-sm-4">
              <h2>{{ $title }}</h2>

              @foreach($cart as $item)
                    <tr>
                        <td class="cart_product">
                            <a href=""><img src="images/{{$item->file_name}}" class="img-rounded" alt="Cinque Terre" width="100" height="auto"></a>
                        </td>
                        <td class="cart_description">
                            <h4><a href="">{{$item->name}}</a></h4>
                            <p>Web ID: {{$item->id}}</p>
                        </td>
                        <td class="cart_price">
                            <p>${{$item->price}}</p>
                        </td>
                    
                        <td class="cart_total">
                            <p class="cart_total_price">${{$item->subtotal}}</p>
                        </td>
                        <td class="cart_delete">
                            <a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
                        </td>
                    </tr>
                    @endforeach
                    @else
                <p>You have no items in the shopping cart</p>
                @endif
            </div>
          </div>
      </div>

@endsection
