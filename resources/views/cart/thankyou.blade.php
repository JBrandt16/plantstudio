@extends('layouts.layout')

@section('content')

<!--
    $order = new stdClass;
    $order->id;
    $order->users_id;
    $order->first_name;
    $order->last_name;
    $order->email;
    $order->phone;
    $order->street;
    $order->city;
    $order->province;
    $order->country;
    $order->postal_code;
    $order->transcation_status;
    $order->shipping_status;
    $order->created_at = \Carbon\Carbon::now();
    $order->updated_at = \Carbon\Carbon::now();

    $line_items = [];
    $line_items[0] = new stdClass;
    $line_items[0]->id = 1;
    $line_items[0]->order_id = 1;
    $line_items[0]->product_id = 20;
    $line_items[0]->name = 'Apple';
    $line_items[0]->price = 20.00;
    $line_items[0]->qty= 10;
    $line_items[0]->created_at = \Carbon\Carbon::now();
    $line_items[0]->updated_at = \Carbon\Carbon::now();

 $line_items[1] = new stdClass;
    $line_items[1]->id = 2;
    $line_items[1]->order_id = 1;
    $line_items[1]->product_id = 25;
    $line_items[1]->name = 'Orange';
    $line_items[1]->price = 25.00;
    $line_items[1]->qty= 3;
    $line_items[1]->created_at = \Carbon\Carbon::now();
    $line_items[1]->updated_at = \Carbon\Carbon::now();

-->



    <div id="main" class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="text-center">
                <i class="fa fa-search-plus pull-left icon"></i>
                <h1>Thank You for Shopping With Us</h1>
                <h2>Your Order Details</h2>

                <h2>Invoice for purchase {{ $order->id }}</h2>
            </div>
            <hr>
            <div class="row">
                <div class="col-xs-12 col-md-4 col-lg-4 pull-left">
                    <div class="panel panel-default height">
                        <div class="panel-heading">Billing Details</div>
                        <div class="panel-body">
                            <strong>Name:</strong> {{ $order->first_name }} {{ $order->last_name }}
                            <br>
                            {{ $order->street }}<br>
                            {{ $order->city}}<br>
                            {{ $order->province }}<br>
                            {{ $order->country }}
                            {{ $order->postal_code }}
                            <br>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-md-4 col-lg-4">
                    <div class="panel panel-default height">
                        <div class="panel-heading">Payment Information</div>
                        <div class="panel-body">
                            <strong>Card Type: </strong>
                             {{ $transaction->card_type }}<br>
                            <strong>Card Number:</strong>
                            <?php echo '**** **** **** ' . substr($transaction->card_number, -4)?><br>
                            <strong>Expiry Date:</strong>
                            {{ $transaction->expiry_date }}

                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-md-4 col-lg-4 pull-right">
                    <div class="panel panel-default height">
                        <div class="panel-heading">Shipping Address</div>
                        <div class="panel-body">
                            <strong>Name: </strong>{{ $order->first_name }} <br>
                            <strong>Address:</strong> {{ $order->street }}<br>
                            <strong>City:</strong> {{ $order->city }}<br>
                            <strong>Province:</strong> {{ $order->province }}<br>
                            <strong>Postal Code:</strong> {{ $order->postal_code }}<br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="text-center"><strong>Order summary</strong></h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <td><strong>Item Name</strong>
                                    </td>
                                    <td class="text-center"><strong>Item Price</strong></td>
                                    <td class="text-center"><strong>Item Quantity</strong></td>
                                    <td class="text-right"><strong>Total</strong></td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($cart as $item)
                                <tr>
                                    <td>{{ ucwords(str_replace('_', ' ', $item['name'])) }}</td>
                                    <td class="text-center">&dollar;{{money_format("%.2n", $item['price'])}}
                                    </td>
                                    <td class="text-center">{{ $item['qty'] }}</td>
                                    <td class="text-right">&dollar;{{money_format("%.2n", $item['price']*$item['qty'])}}</td>
                                </tr>
                                @endforeach

                                <tr>
                                    <td class="highrow"></td>
                                    <td class="highrow"></td>
                                    <td class="highrow text-center"><strong>Subtotal</strong></td>
                                    <td class="highrow text-right">&dollar;{{money_format("%.2n", $order->subtotal)}}</td>
                                </tr>
                                <tr>
                                    <td class="emptyrow"></td>
                                    <td class="emptyrow"></td>

                                    <td class="emptyrow text-center"><strong>GST</strong></td>
                                    <td class="emptyrow text-right">&dollar;{{money_format("%.2n",
                                        $order->gst)}}</td>

                                </tr>

                                <tr>

                                    <td class="emptyrow"></td>
                                    <td class="emptyrow"></td>
                                    <td class="emptyrow text-center"><strong>PST</strong></td>
                                    <td class="emptyrow text-right">&dollar;{{money_format("%.2n",
                                        $order->pst)}}</td>

                                </tr>

                                <tr>
                                    <td class="emptyrow"></td>
                                    <td class="emptyrow"></td>
                                    <td class="emptyrow text-center"><strong>Total</strong></td>
                                    <td class="emptyrow text-right">&dollar;{{money_format("%.2n",
                                        $order->total)}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
