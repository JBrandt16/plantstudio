@extends('layouts.layout') 

@section('content')

  <div id="main" class="container main_content">

  	@if(count($cart))
  	<div class="row">

  		<div class="col-sm-12">
  			<h2>Shopping Cart</h2>
  		</div><!-- .col-sm-12 -->

  	</div><!-- .row -->

  	

  		@foreach($cart as $item)
<div class="row cart_item">
  			<div class="col-sm-2">
  				<img src="/images/{{$item['image']}}" alt="" width="100" height="100">
  			</div><!-- .col-sm-2 -->

  			<div class="col-sm-2">
  				<strong>{{ucwords(str_replace('_', ' ', $item['name']))}}</strong><br />
  				<span class="label">&dollar;{{money_format("%.2n", $item['line_price'])}} </span>
  			</div><!-- .col-sm-2 -->

  			<div class="col-sm-2">
  				<div class="btn-group">
              {!! Form::open(['url' => '/cart', 'method' => 'patch']) !!} {{csrf_field()}}
              <input type="hidden" name="id" value="{{$item['id']}}">
              <input type="hidden" name="increment" value="add">
              <button type="submit" class="btn btn-success">&#43;</button>
              </form>
            </div>
  			</div><!-- .col-sm-2 -->

  			<div class="col-sm-2">
  				<div class="btn-group">
              {!! Form::open(['url' => '/cart', 'method' => 'patch']) !!} {{csrf_field()}}
              <input type="hidden" name="id" value="{{$item['id']}}">
              <input type="hidden" name="increment" value="minus">
              @if($item['qty'] > 1)
                <button type="submit" class="btn btn-danger">&minus;</button>
              @endif

              </form>
            </div>
  			</div><!-- .col-sm-2 -->

  			<div class="col-sm-2">
  				<div class="btn-group remove-item">
              		<a class="btn btn-danger" href="/cart/{{$item['id']}}">
                		<span class="glyphicon glyphicon-remove"></span> REMOVE ITEM
              		</a>
            	</div>
  			</div><!-- .col-sm-2 -->

  			<div class="col-sm-2">
  				<span class="badge">{{$item['qty']}} </span>
  			</div><!-- .col-sm-2 -->
</div><!-- .row -->
<hr />
  		@endforeach

  	<div class="row">
      <div class="col-sm-12">
        <table class="cart-total">
          <tr>
            <th class="subtotal">Subtotal(CA):</th>
            <td class="subtotal">&dollar;{{money_format("%.2n", $subTotal)}}</td>
          </tr>

          <tr>
            <th class="gst">GST:</th>
            <td class="gst">
              <?=number_format((float)$gst, 2)?>
            </td>
          </tr>

          <tr>
            <th class="pst">PST:</th>
            <td class="pst">
              <?=number_format((float)$pst, 2)?>
            </td>
          </tr>

          <tr>
            <th class="total">Total:</th>
            <td class="total">
              <?=number_format((float)$finalTotal, 2)?>
            </td>
          </tr>
        </table>

        {!! Form::open(['url' => '/payment', 'method' => 'get']) !!}
        <button type="submit" class="btn btn-success checkout"> &nbsp;&nbsp;&nbsp;&nbsp; Checkout &nbsp;&nbsp;&nbsp;&nbsp;</button> {!! Form::close() !!}

      </div>
    </div>

  	@else
	    <div class="empty-cart">
	      <h1>There are currently no items in your shopping cart</h1>
	      <img src="/images/cart.jpg" alt="Shopping Cart" width="200" height="200" />
	    </div>
    @endif
    
  </div>

  @endsection
