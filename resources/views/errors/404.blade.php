@extends('layouts.layout')

@section('content')

	<div class="container main_content">
		<h1 style="text-align: center;">Page Not Found</h1>
		<h2 style="text-align: center;">Please try searching for something else</h2>
		<br />
		<div id='search-box' style="margin: 0 auto">
        <form action='/search' id='search-form' method='get' target='_top'>
          <input id='search-text' name='q' placeholder='Search' type='text' />
          <button id='search-button' type='submit'>                     
            <span>Search</span>
          </button>
        </form>
      </div><!-- .search-box-->

	</div>


@endsection