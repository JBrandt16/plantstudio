  <!--footer-->
      <div id="footer" class="row">
        <footer id="myFooter">
              <div class="container">
                  <div class="row">
                      <div class="col-sm-4">
                          <h5>ADDRESS</h5>
              2336 S 156TH MAIN WATT<br />
              1212 PLASKA WINNIPEG<br />
              1(204)000-9951
                      </div>
                      <div class="col-sm-2">
                          <h5>Get started</h5>
                          <ul>
                              <li><a href="/">Home</a></li>
                              <li><a href="/register">Sign up</a></li>
                              <li><a href="/contact">Contact us</a></li>
                          </ul>
                      </div>
                      <div class="col-sm-2">
                          <h5>Privacy</h5>
                          <ul>
                           <li><a href="/privacy">Privacy Statement</a></li>
                           <li><a href="/terms">Terms & conditions of use</a></li>

                          </ul>
                      </div>
                      <div class="col-sm-2">
                          <h5>Support</h5>
                          <ul>
                              <li><a href="/faq">FAQ</a></li>
                            

                          </ul>
                      </div>
                      <div class="col-sm-2">
                          <div class="social-networks" style="margin-top: 30px">
                               <a href="http://facebook.com" target="blank"><img src="/images/facebook.png" alt="facebook"></a>
                              <a href="http://instagram.com" target="blank"><img src="/images/instagram.png" alt="instagram"></a>
                              <a href="http://pinterest.com" target="blank"><img src="/images/pinterest.png" alt="pinterest"></a>
                          </div>

                      </div>
                  </div>

              </div>

          </footer>
    </div><!-- #footer .row -->
    <script>
    $('.mobile_categories').click(function(){
        $('.mobile_categories_menu').addClass('category_open');
    });
    $('.mobile_categories_menu .close').click(function(){
        $('.mobile_categories_menu').removeClass('category_open');
    });
      $(document).ready(function(){
          $("#paraf").click(function(){
              $("#mainp").slideToggle("slow");
          });
      });

      $(document).ready(function(){
          $("#paraf1").click(function(){
              $("#mainp1").slideToggle("slow");
          });
      });
      $(document).ready(function(){
          $("#paraf2").click(function(){
              $("#mainp2").slideToggle("slow");
          });
      });
</script>
  </body>
</html>
