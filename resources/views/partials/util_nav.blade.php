<div class="row">
	<div id="utility_nav" class="col-sm-12">
	   <a href="#flyerModal" data-toggle="modal" id="flyer">Flyer</a> &nbsp;&vert;

	  @if(Auth::check())
        	<a href="/profile">{{{ Auth::user()->first_name }}}</a> &nbsp;&vert;
        	@if(Auth::user()->is_admin == 1)
        		<a href="/admin">ADMIN</a> &nbsp;&vert;
        	@endif
        	<a href="/logout">LOGOUT</a> &nbsp;&nbsp;
    	@else 
        	<a href="/login">LOGIN </a> &nbsp;&vert;
	  		<a href="/register">REGISTER</a> &nbsp;&nbsp;
    	@endif
	  
	  <a href="/cart"><img src="/images/shopping_cart.png" alt="Shopping Cart" />
      <span class="badge">{{ count(Session::get('cart')) }}</span>
    </a>
	</div>

    <div class="modal fade" id="flyerModal" 
     tabindex="-1" role="dialog" 
     >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" 
          data-dismiss="modal" 
          aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <img src="images/flyer.jpg" alt="PlantStudio Flyer" width="1000" height="771" />
      </div>
      <div class="modal-footer">
        <button type="button" 
           class="btn btn-default" 
           data-dismiss="modal">Close</button>
        <span class="pull-right">
        </span>
      </div>
    </div>
  </div>
</div>
</div>