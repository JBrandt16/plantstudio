<!DOCTYPE html>
<!--
  #**********************************************
  #**********************************************
  # Program   :  Winter Web Development Diploma
  # Course    :  eCommerce PHP ProgrammingProject
  # Instuctor :  Steve George
  # Group Name:  Alex,Gauri,Giancarlo,Jayda,Katie,
  #              Navroop,Pratibha,Stanislav
  #**********************************************
  #**********************************************
-->
<html lang="en">
<head>
  <title>PlantStudio</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  {{Html::script('https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js')}}
  {{Html::script('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js')}}
  {{Html::style('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css')}}
  
  {{Html::script('js/jquery.nivo.slider.pack.js')}}
  {{Html::script('js/slider_function.js')}}
  {{Html::style('css/nivo-slider.css')}}
  {{Html::style('css/nivo-slider.css')}}

  {{Html::style('css/responsive_navbar.css')}}



  {{Html::style('css/index.css')}}
  {{Html::style('css/style.css')}}
  {{Html::style('css/style_description.css')}}
  {{Html::style('css/media.css')}}

  {{Html::style('css/payment.css')}}
  {{Html::style('css/cart.css')}}
  <link rel="shortcut icon" href="{{{ asset('/images/favicon.png') }}}">
</head>
<body>
