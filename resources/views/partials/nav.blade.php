<!-- NEW ONE -->

<nav class="navbar navbar-inverse">
<div class="container">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-3">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
  </div>
    <div class="col-sm-3">
      <div id="logo">
        <a href="/"><img src="/images/plantstudio_logo.png" alt="PlantStudio Logo" /></a>
      </div><!--#logo-->
    </div><!--col-sm-4-->



  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse" id="navbar-collapse-3">
    <ul class="nav navbar-nav navbar-right">
      <li><a href="/">HOME</a></li>
      <li><a href="/plants">PLANTS</a></li>
      <li><a href="/about">ABOUT US</a></li>
      <li><a href="/contact">CONTACT</a></li>
      <li><a href="/faq">FAQ</a></li>
      <li>
        <a class="btn btn-default btn-outline btn-circle collapsed"  data-toggle="collapse" href="#nav-collapse3" aria-expanded="false" aria-controls="nav-collapse3">Search</a>
      </li>
    </ul>
    <div class="collapse nav navbar-nav nav-collapse slide-down" id="nav-collapse3">
      <form class="navbar-form navbar-right" role="search" action='/search' method="get">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search" name="q" />
        </div>
        <button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
      </form>
    </div>
  </div><!-- /.navbar-collapse -->
</div><!-- /.container -->
</nav><!-- /.navbar -->

<!-- END NEW ONE -->
