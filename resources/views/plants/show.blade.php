@extends('layouts.detail_layout')

@section('content')
  <div id="detail_content" class="row">

    <div class="col-md-3">
      <div id="image_box">
        <img src="/images/{{$plant->file_name}}" alt="{{$plant->name}}" class="image_description" width="320" height="320"/>
      </div><!-- #image_box --> 
    </div><!-- .col-md-3 -->
     
    <div id="content" class="col-md-9">
      <h1 id="plants_title">{{ ucwords(str_replace('_',' ',($plant->name))) }}</h1>
    
      <div id="details" class="col-md-12">
        <div id="details_box">
          
         
          <div class="rating">
            <span>☆</span><span>☆</span><span>☆</span><span>☆</span><span>☆</span>
          </div><!-- .rating -->

          <br/>

          <p class="price"><strong>Price: ${{$plant->price}}.00</strong></p>
          <p class="seeds"><strong>Seeds Available: @if($plant->seed == 1) Yes @else No @endif</strong></p>

          
            <form method="post" id="buy" action="/cart">
              <p> 
              {{ csrf_field() }}
              <input type="hidden" name="id" value="{{ $plant->id }}">
              <input type="number" value="1" name="qty" min="1">
              <input type="submit" value="Add to Cart" class="buy_button">
            </p>
            </form>
          

        </div><!-- #details_box -->

        <div id="description" class="col-md-12">
          <h2>Description:</h2>
            <div id="description_box">
              <p>
                {{ $plant->long_description }}
              </p>
            </div><!-- #description_box -->
        </div><!-- .col-md-12 -->
      
        <div id="reviews" class="col-md-12">
          <h2>Reviews:</h2>
          <div id="reviews_box">
            <p>
              @if(!empty($review->contents))
              {{$review->contents}}
              @else
                There are no reviews for the {{$plant->name}} yet.
              @endif

            </p>
          </div><!-- #reviews_box -->
        </div><!-- .col-md-12 -->
     
      </div><!-- .col-md-12 -->
    </div><!-- .col-md-9 -->
  </div><!-- .row -->

@endsection