@extends('layouts.plants_layout')

@section('content')

  <div class="row main_content">
    <div class="col-md-3 col-xs-12" id="category_sidenav">
    <div class="categories">
        <h1>Categories</h1>
        <ul>
          @foreach($categories as $category)
          <li><a href="/categories/{{$category->id}}"><?=ucwords(str_replace('_', ' ', $category->name))?></a></li>
          @endforeach
        </ul>
      </div><!-- .categories -->
    </div><!--.col-md-3 -->

    <div  class="mobile_categories_menu" >
      <a class="close">Close</a>
      <h1>Categories</h1>
      <ul>
        @foreach($categories as $category)
        <li><a href="/categories/{{$category->id}}"><?=ucwords(str_replace('_', ' ', $category->name))?></a></li>
        @endforeach
      </ul>
    </div><!-- .mobile_categories_menu -->

    <button class="mobile_categories" id="mobile_cat">
        Categories
    </button>

    <div class="col-md-9 col-xs-12">
      <h1 class="plants_title">Plants online store</h1>
      <!-- <h4 id="sort_header">Sort products by:</h4>

      <div id="mobile_sort_button">
        <select id="select_box">
          <option value="name">name</option>
          <option value="date">date</option>
          <option value="price:high-low">price:high-low</option>
          <option value="price:low-high">price:low-high</option>
        </select>
      </div>  --><!-- .mobile_sort_button -->

      <div id='search-box'>
        <form action='/search' id='search-form' method='get' target='_top'>
          <input id='search-text' name='q' placeholder='Search' type='text' />
          <button id='search-button' type='submit'>                     
            <span>Search</span>
          </button>
        </form>
      </div><!-- .search-box-->

      <div class="row">
        <div class="col-md-9">
          <div class="allprod" id="product_list"> 
          
            @foreach($plants as $plant)
              
              <a href="/plants/{{$plant->id}}"><div class="product">
                  <h3><?=ucwords(str_replace('_', ' ', $plant->name))?></h3>
                  <img src="/images/{{$plant->file_name}}" class="img-rounded" alt="{{$plant->file_name}}" width="100" height="100">
                  <p>price: ${{number_format($plant->price, 2) }}</p>
                 
                  <div class="btn btn-default btn-sm add_to_cart">
                    Details
                  </div>
                </div></a>
              
            @endforeach

            <div class="row">
              <div class="col-md-12">
                {{ $plants->links() }}
              </div><!-- .col-md-12 -->
            </div><!-- .row -->

          </div><!-- .allprod -->
        </div><!-- .col-md-12 -->
      </div><!-- .row -->

  </div><!-- .col-md-9 -->
</div><!-- .row -->
@endsection
