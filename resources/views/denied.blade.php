@extends('layouts.layout')

	@section('content')
<div class="container main_content">
<h1 id="denied_title">Access Denied!</h1>

  <div class="row">
    <div class="col-sm-12">
      <p id="denied_para">Sorry <strong>{{{ Auth::user()->first_name }}}</strong>, you don't have privileges to access this page.</p>
    </div><!-- .col-sm-12 -->
  </div><!-- .row -->
</div><!-- .container -->
@endsection

