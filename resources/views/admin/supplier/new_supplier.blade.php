@extends('admin.home')

@section('content')
<h1>Add a Supplier</h1>
	<form method="post" action="/admin/supplier_add">
		{{ csrf_field() }}

		<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} row">
	        <label for="name" class="col-sm-2 col-form-label">Name: </label>

	        <div class="col-sm-10">
	            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required>

	            @if ($errors->has('name'))
	                <span class="help-block">
	                    <strong>{{ $errors->first('name') }}</strong>
	                </span>
	            @endif
	        </div>
	    </div>

	    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }} row">
	        <label for="phone" class="col-sm-2 col-form-label">Phone: </label>

	        <div class="col-sm-10">
	            <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" required>

	            @if ($errors->has('phone'))
	                <span class="help-block">
	                    <strong>{{ $errors->first('phone') }}</strong>
	                </span>
	            @endif
	        </div>
	    </div>

	    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} row">
	        <label for="email" class="col-sm-2 col-form-label">Email: </label>

	        <div class="col-sm-10">
	            <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" required>

	            @if ($errors->has('email'))
	                <span class="help-block">
	                    <strong>{{ $errors->first('email') }}</strong>
	                </span>
	            @endif
	        </div>
	    </div>

	  <input type="submit" value="Add New Product" class="btn btn-info" />

	</form>

@endsection