@extends('admin.home')

@section('content')
<h1>{{ ucwords(str_replace('_', ' ', $supplier->name)) }}</h1>

	<form method="post" action="/admin/supplier_update">
		{{ csrf_field() }}
		<input type="hidden" name="id" value="{{ $supplier->id }}" />

	  <div class="form-group row">
	    <label for="name" class="col-sm-2 col-form-label">Name: </label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" name="name" placeholder="{{ $supplier->name }}" value="{{ $supplier->name }}">

	      	@if ($errors->has('name'))
		        <span class="help-block">
		            <strong>{{ $errors->first('name') }}</strong>
		        </span>
		    @endif

	    </div>
	  </div>

	  <div class="form-group row">
	    <label for="phone" class="col-sm-2 col-form-label">Phone: </label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" name="phone" placeholder="{{ $supplier->phone }}" value="{{ $supplier->phone }}" />

	      	@if ($errors->has('phone'))
		        <span class="help-block">
		            <strong>{{ $errors->first('phone') }}</strong>
		        </span>
		    @endif

	    </div>
	  </div>

	  <div class="form-group row">
	    <label for="email" class="col-sm-2 col-form-label">Email: </label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" name="email" placeholder="{{ $supplier->email }}" value="{{ $supplier->email }}" />

	      	@if ($errors->has('email'))
		        <span class="help-block">
		            <strong>{{ $errors->first('email') }}</strong>
		        </span>
		    @endif

	    </div>
	  </div>

	  <input type="submit" value="Apply Changes" />

	</form>

@endsection