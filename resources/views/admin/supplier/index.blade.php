@extends('admin.home')

@section('content')
<h1>Admin Suppliers</h1>

	<table class="table">
		<tr><!-- Table Row 1-->
			<th>Name</th>
			<th>Phone</th>
			<th>Email</th>
			<th></th>
			<th></th>

		</tr><!-- End Table Row 1-->

		@foreach($suppliers as $supplier)

			<tr><!-- Table Row -->

				<td><!-- Table Column 1 -->
					{{ ucwords(str_replace('_', ' ', $supplier->name)) }}
				</td><!-- End Table Column 1 -->

				<td><!-- Table Column 2 -->
					{{ $supplier->phone }}
				</td><!-- End Table Column 2 -->

				<td><!-- Table Column 3 -->
					{{ $supplier->email }}
				</td><!-- End Table Column 3 -->

				<td><!-- Table Column 4 -->
					<a href="/admin/edit-supplier/{{ $supplier->id }}" class="btn btn-warning">Edit</a>
				</td><!-- End Table Column 4 -->

				<td><!-- Table Column 5 -->
					<a href="/admin/delete_supplier/{{$supplier->id}}" class="btn btn-danger">Delete</a>
				</td><!-- End Table Column 5 -->

				

			</tr><!-- End Table Row -->

		@endforeach



	</table>

@endsection