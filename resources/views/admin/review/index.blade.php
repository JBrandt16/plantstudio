@extends('admin.home')

@section('content')
<h1>Admin Reviews</h1>

	<table class="table">
		<tr><!-- Table Row 1-->
			<th>Product</th>
			<th>User</th>
			<th>Title</th>
			<th>Published</th>
			<th></th>
			<th></th>

		</tr><!-- End Table Row 1-->

		@foreach($reviews as $review)

			<tr><!-- Table Row -->

				<td><!-- Table Column 1 -->
					@foreach ($products as $product)
						@if($review->product_id == $product->id)
							{{ $product->name }}
						@endif
					@endforeach
				</td><!-- End Table Column 1 -->

				<td><!-- Table Column 2 -->
					@foreach ($users as $user)
						@if($review->user_id == $user->id)
							{{ $user->first_name }}
						@endif
					@endforeach
				</td><!-- End Table Column 2 -->

				<td><!-- Table Column 3 -->
					{{ $review->title }}
				</td><!-- End Table Column 3 -->

				<td><!-- Table Column 4 -->

					@if($review->published == 1) 
						Yes
					@elseif($review->published == 0)
						No
					@endif

				</td><!-- End Table Column 4 -->

				<td><!-- Table Column 5 -->
					<a href="/admin/reviews/{{ $review->id }}" class="btn btn-warning">Edit</a>
				</td><!-- End Table Column 5 -->

				<td><!-- Table Column 6 -->
						@if($review->published == 0)
							<a href="/admin/delete-review/{{ $review->id }}" class="btn btn-danger">Delete</a>
						@endif
					
				</td><!-- End Table Column 6 -->

				

			</tr><!-- End Table Row -->

		@endforeach



	</table>

@endsection