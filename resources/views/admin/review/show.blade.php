@extends('admin.home')

@section('content')
<h1>Review: {{$review->title}}</h1>

<div class="container">


	<div class="row table-responsive">
		<div class="col-sm-12">

	      <table class="table">
	 		
	 		<tr>
	 			<td>Product: </td>
	 			<td>
	 				@foreach($products as $product)
	 					@if($review->product_id == $product->id)
	 						{{ $product->name }}
	 					@endif
	 				@endforeach
	 			</td>
	 		</tr>
	 		<tr>
	 			<td>User: </td>
	 			<td>
	 				@foreach($users as $user)
	 					@if($review->user_id == $user->id)
	 						{{ $user->first_name }} {{ $user->last_name }}
	 					@endif
	 				@endforeach
	 			</td>
	 		</tr>
	 		<tr>
	 			<td>Content: </td>
	 			<td>{{$review->contents}}</td>
	 		</tr>
	 		
	 		<tr>
	 			<td>Published: </td>
	 			<td>
	 				<form method="post" action="/admin/update_review">
	 					{{ csrf_field() }}
	 					<input type="hidden" name="id" value="{{$review->id}}">
          				<select name="published" id="published">
                			<option value="1" < @if($review->published == 1) selected="yes" @endif> Yes </option>
                								
                    		<option value="0" < @if($review->published == 0)) selected="yes"  @endif> No </option> 
                    	</select>

	 			</td>
	 		</tr>
	 		<tr>
	 			<td><input class="btn btn-primary" type="submit" name="" value="Update"></td>
	 			</form>
	 			@if($review->published == 0)
	 				<td><a class="btn btn-danger" href="/admin/delete_review/{{$review->id}}">Delete</a></td>
	 			@endif
	 			
	 		</tr>

	 	</table>


  
    </div>
</div>
</div>

@endsection