@extends('admin.home')

@section('content')
<h1>Admin Orders</h1>

	<table class="table">
		<tr><!-- Table Row 1-->
			<th>Id</th>
			<th>Subtotal</th>
			<th>GST</th>
			<th>PST</th>
			<th>Total</th>
			<th>User Email</th>
			<th>Transaction Status</th>
			<th>Shipping Status</th>

		</tr><!-- End Table Row 1-->

		@foreach($orders as $order)

			<tr><!-- Table Row -->

				<td><!-- Table Column 1 -->
					{{ $order->id }}
				</td><!-- End Table Column 1 -->

				<td><!-- Table Column 2 -->
					${{ number_format($order->subtotal, 2) }}
				</td><!-- End Table Column 2 -->

				<td><!-- Table Column 3 -->
					${{ number_format($order->gst, 2) }}
				</td><!-- End Table Column 3 -->

				<td><!-- Table Column 4 -->
					${{ number_format($order->pst, 2) }}
				</td><!-- End Table Column 4 -->

				<td><!-- Table Column 3 -->
					${{ number_format($order->total, 2) }}
				</td><!-- End Table Column 3 -->

				<td><!-- Table Column 4 -->
					{{ $order->email }}
				</td><!-- End Table Column 4 -->

				<td><!-- Table Column 5 -->
					{{ $order->transaction_status }}
				</td><!-- End Table Column 5 -->

				<td><!-- Table Column 6 -->
					{{ $order->shipping_status }}
				</td><!-- End Table Column 6 -->

			</tr><!-- End Table Row -->

		@endforeach

	</table>

@endsection