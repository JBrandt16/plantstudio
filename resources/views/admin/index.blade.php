@extends('admin.home')

@section('content')
<div class="container">
	<h1 id="dashboard_title">Admin Dashboard</h1>

	<div class="row">

		<div class="col-sm-6 dashboard_info_box">
			<h2>Number of Users</h2>
			<div class="dashboard_info">{{ $users }}</div>
		</div>

		<div class="col-sm-6 dashboard_info_box">
			<h2>Number of Admin Users</h2>
			<p class="dashboard_info">{{ $admin }}</p>
		</div>

	</div><!-- .row -->

	

	<div class="row dashboard">

		<div class="col-sm-6 dashboard_info_box">
			<h2>Number of Products</h2>
			<div class="dashboard_info">{{ $products }}</div>
		</div>

		<div class="col-sm-6 dashboard_info_box">
			<h2>Number of Categories</h2>
			<p class="dashboard_info">{{ $categories }}</p>
		</div>

	</div><!-- .row -->

	

	<div class="row dashboard">

		<div class="col-sm-6 dashboard_info_box">
			<h2>Average Product Price</h2>
			<div class="dashboard_info">${{ number_format($average_price, 2) }}</div>
		</div>

		<div class="col-sm-6 dashboard_info_box">
			<h2>Highest Product Price</h2>
			<p class="dashboard_info">${{ number_format($highest_price, 2) }}</p>
		</div>

	</div><!-- .row -->

	

	<div class="row dashboard">

		<div class="col-sm-6 dashboard_info_box">
			<h2>Lowest Product Price</h2>
			<div class="dashboard_info">${{ number_format($lowest_price, 2) }}</div>
		</div>

		<div class="col-sm-6 dashboard_info_box">
			<h2>Number of Transactions</h2>
			<p class="dashboard_info">{{ $transactions }}</p>
		</div>

	</div><!-- .row -->

	

	<div class="row dashboard">

		<div class="col-sm-6 dashboard_info_box">
			<h2>Number of Suppliers</h2>
			<div class="dashboard_info">{{ $suppliers }}</div>
		</div>

		<div class="col-sm-6 dashboard_info_box">
			<h2>Number of Orders</h2>
			<p class="dashboard_info">{{ $orders }}</p>
		</div>

	</div><!-- .row -->

	
</div><!-- .container -->
@endsection