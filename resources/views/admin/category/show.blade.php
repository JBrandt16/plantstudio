@extends('admin.home')

@section('content')
<h1>{{ ucwords(str_replace('_', ' ', $category->name)) }}</h1>

	<form method="post" action="/admin/category_update">
		{{ csrf_field() }}
		<input type="hidden" name="id" value="{{ $category->id }}" />

	  <div class="form-group row">
	    <label for="name" class="col-sm-2 col-form-label">Name: </label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" name="name" placeholder="{{ $category->name }}" value="{{ $category->name }}">

	      	@if ($errors->has('name'))
		        <span class="help-block">
		            <strong>{{ $errors->first('name') }}</strong>
		        </span>
		    @endif

	    </div>
	  </div>

	  

	  <input type="submit" value="Apply Changes" class="btn btn-primary" />

	</form>

@endsection