@extends('admin.home')

@section('content')
<h1>Admin Categories</h1>

	<table class="table">
		<tr><!-- Table Row 1-->
			<th>Id</th>
			<th>Name</th>
			<th></th>
			<th></th>

		</tr><!-- End Table Row 1-->

		@foreach($categories as $category)

			<tr><!-- Table Row -->

				<td><!-- Table Column 1 -->
					{{ $category->id }}
				</td><!-- End Table Column 1 -->

				<td><!-- Table Column 2 -->
					{{ ucwords(str_replace('_', ' ', $category->name)) }}
				</td><!-- End Table Column 2 -->

				<td><!-- Table Column 3 -->
					<a href="/admin/edit-category/{{ $category->id }}" class="btn btn-warning">Edit</a>
				</td><!-- End Table Column 3 -->

				<td><!-- Table Column 4 -->
					<a href="/admin/delete_category/{{$category->id}}" class="btn btn-danger">Delete</a>
				</td><!-- End Table Column 4 -->

				

			</tr><!-- End Table Row -->

		@endforeach



	</table>

@endsection