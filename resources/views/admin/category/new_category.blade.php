@extends('admin.home')

@section('content')
<h1>Add a Category</h1>
	<form method="post" action="/admin/category_add">
		{{ csrf_field() }}

		<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} row">
	        <label for="name" class="col-sm-2 col-form-label">Name: </label>

	        <div class="col-sm-10">
	            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

	            @if ($errors->has('name'))
	                <span class="help-block">
	                    <strong>{{ $errors->first('name') }}</strong>
	                </span>
	            @endif
	        </div>
	    </div>

	    

	  <input type="submit" value="Add New Category" class="btn btn-info" />

	</form>

@endsection