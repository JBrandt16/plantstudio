<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="{{{ asset('/images/favicon.png') }}}">

    <title>PlantStudio Admin</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/css/dashboard.css" rel="stylesheet">
    <style>
      
    </style>
  </head>

  <body>
    <header>
      <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="/admin">Dashboard</a>
        <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse hidden-md" id="navbarsExampleDefault" style="@media min-width:768px){.navbar-expand-md .navbar-collapse {display: hidden!important;}}">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
              <a class="nav-link" href="/">PlantStudio</a>
            </li>
            <li class="nav-item">
              <a class="nav-link {{ Request::is('admin/products') ? 'active' : '' }}" href="{{ URL::to('/admin/products') }}">Products</a>
              <ul>
                <li class="nav-item"><a class="nav-link {{ Request::is('admin/add-product') ? 'active' : '' }}" href="{{ URL::to('/admin/add-product') }}">Add Product</a></li>
              </ul>
            </li>
            <li class="nav-item">
              <a class="nav-link {{ Request::is('admin/users') ? 'active' : '' }}" href="{{ URL::to('/admin/users') }}">Users</a>
            </li>
            <li class="nav-item">
              <a class="nav-link {{ Request::is('admin/category') ? 'active' : '' }}" href="{{ URL::to('/admin/category') }}">Categories</a>
              <ul>
                <li class="nav-item"><a class="nav-link {{ Request::is('admin/add-category') ? 'active' : '' }}" href="{{ URL::to('/admin/add-category') }}">Add Category</a></li>
              </ul>
            </li>
            <li class="nav-item">
              <a class="nav-link {{ Request::is('admin/orders') ? 'active' : '' }}" href="{{ URL::to('/admin/orders') }}">Orders</a>
            </li>
            <li class="nav-item">
              <a class="nav-link {{ Request::is('admin/reviews') ? 'active' : '' }}" href="{{ URL::to('/admin/reviews') }}">Reviews</a>
            </li>
            <li class="nav-item">
              <a class="nav-link {{ Request::is('admin/supplier') ? 'active' : '' }}" href="{{ URL::to('/admin/supplier') }}">Suppliers</a>
              <ul>
                <li class="nav-item"><a class="nav-link {{ Request::is('admin/add-supplier') ? 'active' : '' }}" href="{{ URL::to('/admin/add-supplier') }}">Add Supplier</a></li>
              </ul>
            </li>
            
          </ul>
        </div>
      </nav>
    </header>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-sm-3 col-md-2 d-none d-sm-block bg-light sidebar">
          <ul class="nav nav-pills flex-column">
            <li class="nav-item">
              <a class="nav-link" href="/">PlantStudio</a>
            </li>
            <li class="nav-item">
              <a class="nav-link {{ Request::is('admin') ? 'active' : '' }}" href="{{ URL::to('/admin') }}">Admin Dashboard <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link {{ Request::is('admin/products') ? 'active' : '' }}" href="{{ URL::to('/admin/products') }}">Products</a>
              <ul>
                <li class="nav-item"><a class="nav-link {{ Request::is('admin/add-product') ? 'active' : '' }}" href="{{ URL::to('/admin/add-product') }}">Add Product</a></li>
              </ul>
            </li>
            <li class="nav-item">
              <a class="nav-link {{ Request::is('admin/users') ? 'active' : '' }}" href="{{ URL::to('/admin/users') }}">Users</a>
            </li>
            <li class="nav-item">
              <a class="nav-link {{ Request::is('admin/category') ? 'active' : '' }}" href="{{ URL::to('/admin/category') }}">Categories</a>
              <ul>
                <li class="nav-item"><a class="nav-link {{ Request::is('admin/add-category') ? 'active' : '' }}" href="{{ URL::to('/admin/add-category') }}">Add Category</a></li>
              </ul>
            </li>
            <li class="nav-item">
              <a class="nav-link {{ Request::is('admin/orders') ? 'active' : '' }}" href="{{ URL::to('/admin/orders') }}">Orders</a>
            </li>
            <li class="nav-item">
              <a class="nav-link {{ Request::is('admin/reviews') ? 'active' : '' }}" href="{{ URL::to('/admin/reviews') }}">Reviews</a>
            </li>
            <li class="nav-item">
              <a class="nav-link {{ Request::is('admin/supplier') ? 'active' : '' }}" href="{{ URL::to('/admin/supplier') }}">Suppliers</a>
              <ul>
                <li class="nav-item"><a class="nav-link {{ Request::is('admin/add-supplier') ? 'active' : '' }}" href="{{ URL::to('/admin/add-supplier') }}">Add Supplier</a></li>
              </ul>
            </li>
          </ul>

        </nav>

        <main class="col-sm-9 ml-sm-auto col-md-10 pt-3 admin_main">
          

          @yield('content')
        </main>
        </div><!-- .row -->
      </div><!-- .container-fluid -->



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="/js/popper.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
  </body>
</html>
