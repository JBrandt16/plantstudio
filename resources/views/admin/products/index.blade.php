@extends('admin.home')

@section('content')
<h1>Admin Products</h1>

	<table class="table">
		<tr><!-- Table Row 1-->
			<th>Image</th>
			<th>Name</th>
			<th>Price</th>
			<th>Published</th>
			<th></th>
			<th></th>

		</tr><!-- End Table Row 1-->

		@foreach($plants as $plant)

			<tr><!-- Table Row -->

				<td><!-- Table Column 1 -->
					<img src="/images/{{ $plant->file_name }}" alt="{{ $plant->name }}" width="100" height="100" />
				</td><!-- End Table Column 1 -->

				<td><!-- Table Column 2 -->
					{{ ucwords(str_replace('_', ' ', $plant->name)) }}
				</td><!-- End Table Column 2 -->

				<td><!-- Table Column 3 -->
					${{ number_format($plant->price,2) }}
				</td><!-- End Table Column 3 -->

				<td><!-- Table Column 4 -->
					@if($plant->published == 1)
						<p>Yes</p>
					@else
						<p>No</p>
					@endif
				</td><!-- End Table Column 4 -->

				<td><!-- Table Column 5 -->
					<a href="/admin/edit-product/{{ $plant->id }}" class="btn btn-warning">Edit</a>
				</td><!-- End Table Column 5 -->

				<td><!-- Table Column 6 -->
					@if($plant->published == 0)
						<a href="/admin/delete-product/{{ $plant->id }}" class="btn btn-danger">Delete</a>
					@endif
					
				</td><!-- End Table Column 6 -->

				

			</tr><!-- End Table Row -->

		@endforeach



	</table>

@endsection