@extends('admin.home')

@section('content')
<h1>Add a Product</h1>
	<form method="post" action="/admin/product_add">
		{{ csrf_field() }}

		<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} row">
	        <label for="name" class="col-sm-2 col-form-label">Name: </label>

	        <div class="col-sm-10">
	            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

	            @if ($errors->has('name'))
	                <span class="help-block">
	                    <strong>{{ $errors->first('name') }}</strong>
	                </span>
	            @endif
	        </div>
	    </div>

	    <div class="form-group row">
	    <label class="col-sm-2 col-form-label">Category: </label>
	    <div class="col-sm-10">

	    	
	    		@foreach($categories as $category)
          			<input id="{{ $category->name }}" type="checkbox" name="categories[]" value="{{ ucwords(str_replace('_', ' ', $category->id)) }}" />{{ ucwords(str_replace('_', ' ', $category->name)) }}
          			<br />
           		@endforeach
           	
	    </div>
	  </div>

	    <div class="form-group{{ $errors->has('file_name') ? ' has-error' : '' }} row">
	        <label for="file_name" class="col-sm-2 col-form-label">File Name: </label>

	        <div class="col-sm-10">
	            <input id="file_name" type="text" class="form-control" name="file_name" value="{{ old('file_name') }}" required>

	            @if ($errors->has('file_name'))
	                <span class="help-block">
	                    <strong>{{ $errors->first('file_name') }}</strong>
	                </span>
	            @endif
	        </div>
	    </div>

	    <div class="form-group{{ $errors->has('seed') ? ' has-error' : '' }} row">
	        <label for="seed" class="col-sm-2 col-form-label">Seed: </label>

	        <div class="col-sm-10">
	            <select id="seed" name="seed">
          			<option value="1"> Yes </option>
           			<option value="0"> No </option> 
           		</select>

	            @if ($errors->has('seed'))
	                <span class="help-block">
	                    <strong>{{ $errors->first('seed') }}</strong>
	                </span>
	            @endif
	        </div>
	    </div>

	    <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }} row">
	        <label for="price" class="col-sm-2 col-form-label">Price: </label>

	        <div class="col-sm-10">
	            <input id="price" type="text" class="form-control" name="price" value="{{ old('price') }}" required>

	            @if ($errors->has('price'))
	                <span class="help-block">
	                    <strong>{{ $errors->first('price') }}</strong>
	                </span>
	            @endif
	        </div>
	    </div>

	    <div class="form-group{{ $errors->has('quantity') ? ' has-error' : '' }} row">
	        <label for="quantity" class="col-sm-2 col-form-label">Quantity: </label>

	        <div class="col-sm-10">
	            <input id="quantity" type="number" class="form-control" name="quantity" value="{{ old('quantity') }}" max="200" min="0" required>

	            @if ($errors->has('quantity'))
	                <span class="help-block">
	                    <strong>{{ $errors->first('quantity') }}</strong>
	                </span>
	            @endif
	        </div>
	    </div>

	    <div class="form-group{{ $errors->has('short_description') ? ' has-error' : '' }} row">
	        <label for="short_description" class="col-sm-2 col-form-label">Short Description: </label>

	        <div class="col-sm-10">
	            <textarea id="short_description" class="form-control" name="short_description" required>{{ old('short_description') }}</textarea>

	            @if ($errors->has('short_description'))
	                <span class="help-block">
	                    <strong>{{ $errors->first('short_description') }}</strong>
	                </span>
	            @endif
	        </div>
	    </div>

	    <div class="form-group{{ $errors->has('long_description') ? ' has-error' : '' }} row">
	        <label for="long_description" class="col-sm-2 col-form-label">Long Description: </label>

	        <div class="col-sm-10">
	            <textarea id="long_description" class="form-control" name="long_description" required>{{ old('long_description') }}</textarea>

	            @if ($errors->has('long_description'))
	                <span class="help-block">
	                    <strong>{{ $errors->first('long_description') }}</strong>
	                </span>
	            @endif
	        </div>
	    </div>

	  	<div class="form-group{{ $errors->has('supplier_id') ? ' has-error' : '' }} row">
	        <label for="supplier_id" class="col-sm-2 col-form-label">Supplier Id: </label>

	        <div class="col-sm-10">
	            <select id="supplier_id" name="supplier_id">
		    		@foreach($suppliers as $supplier)
	          			<option value="{{$supplier->id}}"> {{$supplier->name}} </option>
	           		@endforeach
           		</select>

	            @if ($errors->has('supplier_id'))
	                <span class="help-block">
	                    <strong>{{ $errors->first('supplier_id') }}</strong>
	                </span>
	            @endif
	        </div>
	    </div>

	    <div class="form-group{{ $errors->has('published') ? ' has-error' : '' }} row">
	        <label for="published" class="col-sm-2 col-form-label">Published: </label>

	        <div class="col-sm-10">
	            <select id="published" name="published">
          			<option value="1"> Yes </option>
           			<option value="0"> No </option> 
           		</select>

	            @if ($errors->has('published'))
	                <span class="help-block">
	                    <strong>{{ $errors->first('published') }}</strong>
	                </span>
	            @endif
	        </div>
	    </div>

	  <input type="submit" value="Add New Product" />

	</form>

@endsection