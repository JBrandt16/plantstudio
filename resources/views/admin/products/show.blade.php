@extends('admin.home')

@section('content')
<h1>{{ ucwords(str_replace('_', ' ', $plant->name)) }}</h1>

	<form method="post" action="/admin/product_update">
		{{ csrf_field() }}
		<input type="hidden" name="id" value="{{ $plant->id }}" />

	  <div class="form-group row">
	    <label for="name" class="col-sm-2 col-form-label">Name: </label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" name="name" placeholder="{{ $plant->name }}" value="{{ $plant->name }}">

		    @if ($errors->has('name'))
		        <span class="help-block">
		            <strong>{{ $errors->first('name') }}</strong>
		        </span>
		    @endif
		    
	    </div>
	  </div>

	  <div class="form-group row">
	    <label for="category_id" class="col-sm-2 col-form-label">Category: </label>
	    <div class="col-sm-10">

	    	
	    		@foreach($categories as $category)
          			<input type="checkbox" name="categories[]" value="{{ ucwords(str_replace('_', ' ', $category->id)) }}" @foreach($cat_prod as $cp)@if($cp->product_id == $plant->id && $cp->category_id == $category->id) checked="checked" @endif @endforeach />{{ ucwords(str_replace('_', ' ', $category->name)) }}
          			<br />
           		@endforeach
           	
	    </div>
	  </div>

	  <div class="form-group row">
	    <label for="file_name" class="col-sm-2 col-form-label">File Name: </label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" name="file_name" placeholder="{{ $plant->file_name }}" value="{{ $plant->file_name }}" />

	      	@if ($errors->has('file_name'))
		        <span class="help-block">
		            <strong>{{ $errors->first('file_name') }}</strong>
		        </span>
		    @endif

	    </div>
	  </div>

	  <div class="form-group row">
	    <label for="seed" class="col-sm-2 col-form-label">Seed: </label>
	    <div class="col-sm-10">
	    	<select name="seed">
          		<option value="1" @if($plant->seed == 1) selected="yes" @endif> Yes </option>
           		<option value="0" @if($plant->seed == 0) selected="yes"  @endif> No </option> 
           	</select>	
	    </div>
	  </div>

	  <div class="form-group row">
	    <label for="price" class="col-sm-2 col-form-label">Price: </label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" name="price" placeholder="{{ number_format($plant->price, 2) }}" value="{{ number_format($plant->price, 2) }}" />

	      	@if ($errors->has('price'))
		        <span class="help-block">
		            <strong>{{ $errors->first('price') }}</strong>
		        </span>
		    @endif

	    </div>
	  </div>

	  <div class="form-group row">
	    <label for="quantity" class="col-sm-2 col-form-label">Quantity: </label>
	    <div class="col-sm-10">
	      <input type="number" class="form-control" name="quantity" placeholder="{{ $plant->quantity }}" value="{{ $plant->quantity }}" max="20" min="0" />
	    </div>
	  </div>

	  <div class="form-group row">
	    <label for="short_description" class="col-sm-2 col-form-label">Short Description: </label>
	    <div class="col-sm-10">
	      <textarea type="text" class="form-control" name="short_description" placeholder="{{ $plant->short_description }}">{{ $plant->short_description }}</textarea>
	      	
	      	@if ($errors->has('short_description'))
		        <span class="help-block">
		            <strong>{{ $errors->first('short_description') }}</strong>
		        </span>
		    @endif

	    </div>
	  </div>

	  <div class="form-group row">
	    <label for="long_description" class="col-sm-2 col-form-label">Long Description: </label>
	    <div class="col-sm-10">
	      <textarea type="text" class="form-control" name="long_description" placeholder="{{ $plant->long_description }}">{{ $plant->long_description }}</textarea>

	      	@if ($errors->has('long_description'))
		        <span class="help-block">
		            <strong>{{ $errors->first('long_description') }}</strong>
		        </span>
		    @endif

	    </div>
	  </div>

	  <div class="form-group row">
	    <label for="supplier_id" class="col-sm-2 col-form-label">Supplier: </label>
	    <div class="col-sm-10">

	    	<select name="supplier_id">
	    		@foreach($suppliers as $supplier)
          			<option value="{{$supplier->id}}" @if($supplier->id == $plant->supplier_id) selected="yes" @endif> {{$supplier->name}} </option>
           		@endforeach
           	</select>
	    </div>
	  </div>

	  <div class="form-group row">
	    <label for="published" class="col-sm-2 col-form-label">Published: </label>
	    <div class="col-sm-10">
	    	<select name="published">
          		<option value="1" @if($plant->published == 1) selected="yes" @endif> Yes </option>
           		<option value="0" @if($plant->published == 0) selected="yes"  @endif> No </option> 
           	</select>
	    </div>
	  </div>

	  <input type="submit" value="Apply Changes" />

	</form>

@endsection