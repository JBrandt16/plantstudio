@extends('admin.home')

@section('content')
<h1>Users</h1>

<div class="container">


	<div class="row table-responsive">
		<div class="col-sm-12">

	      <table class="table">

	      	<tr>
	      		<th>Name</th>
	      		<th>Email:</th>
	      		<th>Admin:</th>
	      		<th>&nbsp;</th>
	      	</tr>

			@foreach($users as $user)
				<tr>
		        	<td>{{$user->first_name}} {{$user->last_name}}</td>

		        	<td>{{$user->email}}</td>

		        	<td>
		        		@if( ($user->is_admin) == 1) Yes
		        		@else No
		        		@endif
		        	</td>
	          		<td><a class="btn btn-warning" href="/admin/users/{{$user->id}}">Edit</a></td>
		      	</tr>
		    @endforeach

		   </table>
    	</div>
	</div>
</div>

@endsection