@extends('admin.home')

@section('content')
<h1>{{$user->first_name}} {{$user->last_name}}</h1>

<div class="container">


	<div class="row table-responsive">
		<div class="col-sm-12">

	      <table class="table">
	 		<tr>
	 			<th colspan="2">{{$user->email}}</th>
	 		</tr>
	 		<tr>
	 			<td>Phone Number: </td>
	 			<td>{{$user->phone}}</td>
	 		</tr>
	 		<tr>
	 			<td>Street: </td>
	 			<td>{{$user->street}}</td>
	 		</tr>
	 		<tr>
	 			<td>City: </td>
	 			<td>{{$user->city}}</td>
	 		</tr>
	 		<tr>
	 			<td>Province: </td>
	 			<td>{{$user->province}}</td>
	 		</tr>
	 		<tr>
	 			<td>Country: </td>
	 			<td>{{$user->country}}</td>
	 		</tr>
	 		<tr>
	 			<td>Postal Code: </td>
	 			<td>{{$user->postal_code}}</td>
	 		</tr>
	 		<tr>
	 			<td>Admin User: </td>
	 			<td>
	 				<form method="post" action="/admin/update_user">
	 					{{ csrf_field() }}
	 					<input type="hidden" name="id" value="{{$user->id}}">
          				<select name="is_admin" id="is_admin">
                			<option value="1" < @if($user->is_admin == 1) selected="yes" @endif> Yes </option>
                								
                    		<option value="0" < @if($user->is_admin == 0)) selected="yes"  @endif> No </option> 
                    	</select>

	 			</td>
	 		</tr>
	 		<tr>
	 			<td><input class="btn btn-primary" type="submit" name="" value="Update"></td>
	 			</form>
	 			<td><a class="btn btn-danger" href="/admin/delete_user/{{$user->id}}">Delete</a></td>
	 			
	 		</tr>

	 	</table>


  
    </div>
</div>
</div>

@endsection