@include('partials.head')

  @include('partials.util_nav')

  <div class="row">

    @include('partials.nav')

  </div><!--#header row-->

  @include('partials.slider')


@yield('content')

@include('partials.footer')
