@extends('layouts.master')

@section('content')

	<div class="container main_content"><!--main content -->

	  <div class="row">
        <div class="col-md-12">
          <div class="allprod_main" id="product_list"> 
          <?php $count = 0; ?>
		  @foreach($plants as $plant)
		    
		    <a href="/plants/{{$plant->id}}"><div class="product">
		        <h3><?=ucwords(str_replace('_', ' ', $plant->name))?></h3>
		        <img src="/images/{{$plant->file_name}}" class="img-rounded" alt="{{$plant->file_name}}" width="100" height="100">
		        <p>price: ${{$plant->price}}.00</p>
		       
		        <div class="btn btn-default btn-sm add_to_cart">
		          Details
		        </div>
		      </div></a>
		    <?php $count++; if($count==14) break; ?>
		  @endforeach

          </div><!-- .allprod -->
        </div><!-- .col-md-12 -->
      </div><!-- .row -->
	  
	  <!-- button -->
	  	<div class="row">
		    <div id="view_plants" class="col-sm-12">
		      <a href="/plants" class="btn-lg btn-primary">VIEW ALL PLANTS &gt;&gt;</a>
		    </div><!-- .col-sm-12 -->
		</div><!-- .row -->

	</div><!-- #container -->

@endsection