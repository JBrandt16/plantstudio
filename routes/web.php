<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/', 'PagesController@index');

Route::get('/about', 'PagesController@about');

Route::get('/contact', 'PagesController@contact');
Route::post('/contact', 'PagesController@send');

Route::get('/faq', 'PagesController@project');

Route::post('/payment', 'PaymentsController@p_form');
Route::get('/payment', 'PaymentsController@g_form');

Route::get('/plants', 'PlantsController@index');

Route::get('/plants/{plant}', 'PlantsController@show');
Route::get('/search', 'PlantsController@search');

Route::get('/privacy', 'PagesController@privacy');
Route::get('/terms', 'PagesController@terms');

/*Category route to plants page*/
Route::get('/categories/{category}', 'PlantsController@category');

// Auth - Register/Login/Logout/profile -
Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/change_pass', 'Auth\ChangePasswordController@show');
Route::post('/send_pass', 'Auth\ChangePasswordController@store');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/profile', 'Auth\ProfileController@show');
Route::get('/profile/edit', 'Auth\ProfileController@edit');
Route::post('/profile', 'Auth\ProfileController@update');
// Auth - end

// Admin section -
Route::get('/admin', 'Admin\DashboardController@home');

Route::get('/admin/users', 'Admin\UserController@users');
Route::get('/admin/users/{id}', 'Admin\UserController@show_user');
Route::post('/admin/update_user', 'Admin\UserController@update_user');
Route::get('/admin/delete_user/{id}', 'Admin\UserController@delete_user');

Route::get('/admin/products', 'Admin\ProductController@products');
Route::get('/admin/edit-product/{id}', 'Admin\ProductController@edit_product');
Route::post('/admin/product_update', 'Admin\ProductController@update_product');
Route::get('/admin/add-product', 'Admin\ProductController@add_product');
Route::post('/admin/product_add', 'Admin\ProductController@store_products');
Route::get('/admin/delete-product/{id}', 'Admin\ProductController@delete_product');

Route::get('/admin/category', 'Admin\CategoryController@categories');
Route::get('/admin/edit-category/{id}', 'Admin\CategoryController@edit_category');
Route::post('/admin/category_update', 'Admin\CategoryController@update_category');
Route::get('/admin/add-category', 'Admin\CategoryController@add_category');
Route::post('/admin/category_add', 'Admin\CategoryController@store_category');
Route::get('/admin/delete_category/{id}', 'Admin\CategoryController@delete_category');

Route::get('/admin/supplier', 'Admin\SupplierController@suppliers');
Route::get('/admin/edit-supplier/{id}', 'Admin\SupplierController@edit_supplier');
Route::post('/admin/supplier_update', 'Admin\SupplierController@update_supplier');
Route::get('/admin/add-supplier', 'Admin\SupplierController@add_supplier');
Route::post('/admin/supplier_add', 'Admin\SupplierController@store_supplier');
Route::get('/admin/delete_supplier/{id}', 'Admin\SupplierController@delete_supplier');

Route::get('/admin/reviews', 'Admin\ReviewController@reviews');
Route::get('/admin/reviews/{id}', 'Admin\ReviewController@show_review');
Route::post('/admin/update_review', 'Admin\ReviewController@update_review');
Route::get('/admin/delete_review/{id}', 'Admin\ReviewController@delete_review');

Route::get('/admin/orders', 'Admin\OrderController@orders');
// Admin - end

Route::get('/home', 'HomeController@index')->name('home');


Route::post('/contact_2', function(){
    Mail::raw('Sending emails with Mailgun and Laravel is easy!', function($message)
    {
        $message->subject('Mailgun and Laravel are awesome!');
        $message->from('sandbox160d8fa063c041b8a59c5a5f058ad6b9.mailgun.org', 'Website Name');
        $message->to('stan291993@@gmail.com');
    });
});

/* Routes for Cart */
Route::post('/cart', 'CartsController@store');
Route::get('/cart', 'CartsController@show');
Route::get('/cart', 'CartsController@subTotal');

Route::get('/cart/{id}', [
          'uses' => 'CartsController@delete',
          'as' => 'remove.item'
]);
Route::patch('/cart', [
          'uses' => 'CartsController@update',
          'as' => 'update.item'
]);
/* End Routes for Cart */